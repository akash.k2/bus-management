import registerService from "../Auth/services/registerService.js";
import addTripService from "../Trips/services/addTripService.js";
import bookTripService from "../Bookings/services/bookTripService.js";

let user;
let travels;
let trip_id;

export const userRegisterMock = async () => {
  user = await registerService(
    "test-user",
    "testuser@gmail.com",
    "Test12345$",
    "user",
    "9988776655"
  );

  return user;
};

export const travelsRegisterMock = async () => {
  travels = await registerService(
    "test-travels",
    "testtravels@gmail.com",
    "Test12345$",
    "travels",
    "9988776677"
  );

  return travels;
};

export const addTripMock = async () => {
  trip_id = await addTripService(
    travels.details.id.toString(),
    "Chennai",
    "Madurai",
    "23:00",
    8,
    30,
    travels.details.id.toString(),
    [0, 1, 2, 3, 4, 5, 6],
    700,
    travels.details.role
  );

  return trip_id;
};

export const bookTripMock = async () => {
  const booking = await bookTripService(
    user.details.id.toString(),
    new Date().toISOString(),
    trip_id.toString(),
    [
      {
        name: "Akash",
        age: "20",
      },
      {
        name: "Anand",
        age: "18",
      },
    ],
    [1, 2],
    "SRK Travels",
    "3000",
    user.details.role
  );

  return booking;
};
