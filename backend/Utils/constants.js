export const USER = "user";

export const TRAVELS = "travels";

//2XX status
export const CREATE_TRIP_SUCCESS = {
  status: 201,
  message: "Trip created successfully",
};

export const REGISTER_SUCCESS = {
  status: 201,
  message: "Registered successfully",
};

export const LOGIN_SUCCESS = {
  status: 200,
  message: "Logged in successfully",
};

export const DELETE_TRIP_SUCCESS = {
  status: 200,
  message: "Trip deleted successfully",
};

export const UPDATE_TRIP_SUCCESS = {
  status: 200,
  message: "Trip updated successfully",
};

export const BOOKING_SUCCESS = {
  status: 201,
  message: "Booking done successfully",
};

export const CANCEL_BOOKING_SUCCESS = {
  status: 200,
  message: "Booking cancelled successfully",
};

export const FETCH_SUCCESS = {
  status: 200,
};

export const NO_CONTENT = {
  status: 204,
  message: "No content available",
};

//4XX status

export const INVALID_TRIP_ID = {
  status: 400,
  message: "Invalid Trip ID",
};

export const INVALID_TRAVELS_ID = {
  status: 400,
  message: "Invalid Travels ID",
};

export const INVALID_BOOKING_ID = {
  status: 400,
  message: "Invalid Booking ID",
};

export const INVALID_USER_ID = {
  status: 400,
  message: "Invalid User ID",
};

export const INVALID_ID_VALUE = {
  status: 400,
  message: "Invalid ID value",
};

export const EMAIL_ALREADY_EXIST = {
  status: 400,
  message: "Email is already taken",
};

export const SEAT_ALREADY_BOOKED = {
  status: 400,
  message: "Seats are already booked. Kindly select other seats",
};

export const SEATS_COUNT_UNAVAILABLE = {
  status: 400,
  message: "Required amount of seats not available",
};

export const INVALID_DATE = {
  status: 400,
  message: "Enter valid date",
};

export const UNAUTHORIZED = {
  status: 401,
  message: "Invalid credentials",
};

export const FORBIDDEN = {
  status: 403,
  message: "Unauthorized",
};

export const NOT_FOUND = {
  status: 404,
  message: "Sorry! Page not found",
};

//5XX status

export const ERROR_UNKNOWN = {
  status: 500,
  message: "Something went wrong!",
};
