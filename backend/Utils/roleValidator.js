import CustomError from "./CustomError.js";
import { FORBIDDEN } from "./constants.js";

const roleValidator = (tokenRole, role) => {
  if (tokenRole !== role) {
    throw new CustomError(FORBIDDEN.message, FORBIDDEN.status);
  } else {
    return true;
  }
};

export default roleValidator;
