import getTrip from "../db/getTrip.js";
import getTravelsBookings from "../db/getTravelsBookings.js";

import globalValidator from "../../Utils/globalValidator.js";
import viewTravelsBookingValidator from "../validators/viewTravelsBookingValidator.js";
import roleValidator from "../../Utils/roleValidator.js";
import { TRAVELS } from "../../Utils/constants.js";

const viewTravelsBookingService = async (date, trip_id, token_role) => {
  if (
    roleValidator(token_role, TRAVELS) &&
    globalValidator(viewTravelsBookingValidator, { date, trip_id })
  ) {
    //checks if the given trip id is valid
    const seat_cap = await getTrip(trip_id);

    //fetches the bookings done on the trip on that particular date
    const result = await getTravelsBookings(date, trip_id, seat_cap);

    return {
      bookings: result.bookings,
      available_seats: result.available_seats,
      seat_cap,
      canceled_bookings: result.canceled_bookings,
    };
  }
};

export default viewTravelsBookingService;
