import getBookedSeatsOnDate from "../db/getBookedSeatsOnDate.js";

import globalValidator from "../../Utils/globalValidator.js";
import getBookedSeatsValidator from "../validators/getBookedSeatsValidator.js";
import roleValidator from "../../Utils/roleValidator.js";
import { USER } from "../../Utils/constants.js";
import getTrip from "../db/getTrip.js";

const getBookedSeatsService = async (trip_id, date, token_role) => {
  if (
    roleValidator(token_role, USER) &&
    globalValidator(getBookedSeatsValidator, { trip_id, date })
  ) {
    //checks validity of trip id
    await getTrip(trip_id);

    //gets booked seats on that date
    const bookedSeats = await getBookedSeatsOnDate(trip_id, date);

    return bookedSeats;
  }
};

export default getBookedSeatsService;
