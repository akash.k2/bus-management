import checkDate from "../utils/checkDate.js";
import getTrip from "../db/getTrip.js";
import getAvailableSeats from "../db/getAvailableSeats.js";
import getBookedSeats from "../db/getBookedSeats.js";
import saveBooking from "../db/saveBooking.js";

import globalValidator from "../../Utils/globalValidator.js";
import roleValidator from "../../Utils/roleValidator.js";
import ticketDetailsValidator from "../validators/ticketDetailsValidator.js";
import seatMapper from "../utils/seatMapper.js";
import checkAvailability from "../utils/checkAvailability.js";
import { USER } from "../../Utils/constants.js";

const bookTripService = async (
  user_id,
  date,
  trip_id,
  passenger_details,
  seatNumbers,
  travels_name,
  total_price,
  token_role
) => {
  if (
    roleValidator(token_role, USER) &&
    globalValidator(ticketDetailsValidator, {
      user_id,
      date,
      trip_id,
      passenger_details,
      seatNumbers,
      travels_name,
      total_price,
    })
  ) {
    //checks if the entered date is correct
    checkDate(date);

    //checks if the given trip id is correct and if true return the seat capacity of the trip
    const seat_cap = await getTrip(trip_id);

    //calculates the number of available seats on that particular date based on the bookings done
    const available_seats = await getAvailableSeats(trip_id, date, seat_cap);

    //checks if the available seats matches the number of requested seats
    checkAvailability(available_seats, seatNumbers.length);

    //checks if the seat numbers requested are free or already booked
    await getBookedSeats(date, trip_id, seatNumbers);

    // maps the seat number with each passenger
    const passDetails = seatMapper(passenger_details, seatNumbers);

    //saves the booking
    const booking = await saveBooking(
      user_id,
      date,
      trip_id,
      passDetails,
      seatNumbers,
      travels_name,
      total_price
    );

    return booking;
  }
};
export default bookTripService;
