import cancelBooking from "../db/cancelBooking.js";

import globalValidator from "../../Utils/globalValidator.js";
import cancelBookingValidator from "../validators/cancelBookingValidator.js";
import roleValidator from "../../Utils/roleValidator.js";
import { USER } from "../../Utils/constants.js";

const cancelBookingService = async (booking_id, token_role) => {
  if (
    roleValidator(token_role, USER) &&
    globalValidator(cancelBookingValidator, { booking_id })
  ) {
    //cancels the booking
    await cancelBooking(booking_id);

    return true;
  }
};

export default cancelBookingService;
