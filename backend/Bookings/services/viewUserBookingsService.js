import getUserBookings from "../db/getUserBookings.js";

import CustomError from "../../Utils/CustomError.js";
import globalValidator from "../../Utils/globalValidator.js";
import viewUserBookingValidator from "../validators/viewUserBookingValidator.js";
import roleValidator from "../../Utils/roleValidator.js";
import { INVALID_USER_ID, USER } from "../../Utils/constants.js";
const viewUserBookingService = async (token_id, user_id, token_role) => {
  if (roleValidator(token_role, USER)) {
    if (token_id !== user_id) {
      throw new CustomError(INVALID_USER_ID.message, INVALID_USER_ID.status);
    } else {
      if (globalValidator(viewUserBookingValidator, { user_id })) {
        //fetches all the bookings done by that user
        const result = await getUserBookings(user_id);

        return result;
      }
    }
  }
};

export default viewUserBookingService;
