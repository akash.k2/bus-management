import dotenv from "dotenv";
dotenv.config();

import mongoose from "mongoose";
import { MongoMemoryServer } from "mongodb-memory-server";

import {
  addTripMock,
  bookTripMock,
  travelsRegisterMock,
  userRegisterMock,
} from "../../Utils/mocks.js";

import viewUserBookingService from "../services/viewUserBookingsService.js";

import { INVALID_USER_ID } from "../../Utils/constants.js";

let user;
let travels;
let user_id;

beforeEach(async () => {
  const mongoServer = await MongoMemoryServer.create();
  const url = mongoServer.getUri();
  await mongoose.connect(url);

  user = await userRegisterMock();
  travels = await travelsRegisterMock();
  await addTripMock();
  await bookTripMock();

  user_id = user.details.id.toString();
});

afterEach(async () => {
  await mongoose.connection.dropDatabase();
  await mongoose.connection.close();
});

describe("View User Booking", () => {
  it("with invalid user id", async () => {
    await viewUserBookingService(
      user_id,
      "64ab9cde2fa614682c011b24",
      user.details.role
    ).catch((err) => {
      expect(err.message).toBe(INVALID_USER_ID.message);
      expect(err.statusCode).toBe(INVALID_USER_ID.status);
    });
  });

  it("with valid user id", async () => {
    const res = await viewUserBookingService(
      user_id,
      user_id,
      user.details.role
    );

    expect(res).toHaveProperty("prevBookings");
    expect(res).toHaveProperty("currBookings");
    expect(res).toHaveProperty("cancelledBookings");
  });
});
