import dotenv from "dotenv";
dotenv.config();

import mongoose from "mongoose";
import { MongoMemoryServer } from "mongodb-memory-server";

import {
  addTripMock,
  bookTripMock,
  travelsRegisterMock,
  userRegisterMock,
} from "../../Utils/mocks.js";

import cancelBookingService from "../services/cancelBookingService.js";

import { INVALID_BOOKING_ID } from "../../Utils/constants.js";

let user;
let travels;
let trip_id;
let booking;

beforeEach(async () => {
  const mongoServer = await MongoMemoryServer.create();
  const url = mongoServer.getUri();
  await mongoose.connect(url);

  user = await userRegisterMock();
  travels = await travelsRegisterMock();
  trip_id = await addTripMock();
  booking = await bookTripMock();
});

afterEach(async () => {
  await mongoose.connection.dropDatabase();
  await mongoose.connection.close();
});

describe("Cancel Booking", () => {
  it("with valid booking id", async () => {
    const booking_id = booking._id.toString();

    const res = await cancelBookingService(booking_id, user.details.role);

    expect(res).toBe(true);
  });

  it("with invalid booking id", async () => {
    const booking_id = booking._id.toString();

    await cancelBookingService(booking_id, user.details.role).catch((err) => {
      expect(err.message).toBe(INVALID_BOOKING_ID.message);
      expect(err.statusCode).toBe(INVALID_BOOKING_ID.status);
    });
  });
});
