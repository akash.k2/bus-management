import dotenv from "dotenv";
dotenv.config();

import mongoose from "mongoose";
import { MongoMemoryServer } from "mongodb-memory-server";

import {
  addTripMock,
  bookTripMock,
  travelsRegisterMock,
  userRegisterMock,
} from "../../Utils/mocks.js";

import bookTripService from "../services/bookTripService.js";

import { SEAT_ALREADY_BOOKED } from "../../Utils/constants.js";

let user;
let trip_id;
let bookingDetails;

beforeEach(async () => {
  const mongoServer = await MongoMemoryServer.create();
  const url = mongoServer.getUri();
  await mongoose.connect(url);

  user = await userRegisterMock();

  await travelsRegisterMock();

  trip_id = await addTripMock();

  await bookTripMock();

  bookingDetails = {
    user_id: user.details.id.toString(),
    date: new Date().toISOString(),
    trip_id: trip_id.toString(),
    passenger_details: [
      {
        name: "Akash",
        age: "20",
      },
      {
        name: "Anand",
        age: "18",
      },
    ],
    seatNumbers: [1, 2],
    travels_name: "SRK Travels",
    total_price: "3000",
  };
});

afterEach(async () => {
  await mongoose.connection.dropDatabase();
  await mongoose.connection.close();
});

describe("Book Trip", () => {
  it("with correct details", async () => {
    const res = await bookTripService(
      bookingDetails.user_id,
      bookingDetails.date,
      bookingDetails.trip_id,
      bookingDetails.passenger_details,
      [3, 4],
      bookingDetails.travels_name,
      bookingDetails.total_price,
      user.details.role
    );

    expect(res).toBeTruthy();
    expect(res).toHaveProperty("_id");
  });

  it("with invalid details", async () => {
    //Seats already booked
    await bookTripService(
      bookingDetails.user_id,
      bookingDetails.date,
      bookingDetails.trip_id,
      bookingDetails.passenger_details,
      bookingDetails.seatNumbers,
      bookingDetails.travels_name,
      bookingDetails.total_price,
      user.details.role
    ).catch((err) => {
      expect(err.message).toBe(SEAT_ALREADY_BOOKED.message);
      expect(err.statusCode).toBe(SEAT_ALREADY_BOOKED.status);
    });
  });
});
