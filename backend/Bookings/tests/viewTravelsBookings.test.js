import dotenv from "dotenv";
dotenv.config();

import mongoose from "mongoose";
import { MongoMemoryServer } from "mongodb-memory-server";

import {
  addTripMock,
  bookTripMock,
  travelsRegisterMock,
  userRegisterMock,
} from "../../Utils/mocks.js";

import viewTravelsBookingService from "../services/viewTravelsBookingsService.js";

import { INVALID_TRIP_ID } from "../../Utils/constants.js";

let travels;
const date = new Date().toISOString();
let trip_id;

beforeEach(async () => {
  const mongoServer = await MongoMemoryServer.create();
  const url = mongoServer.getUri();
  await mongoose.connect(url);

  await userRegisterMock();
  travels = await travelsRegisterMock();
  trip_id = await addTripMock();
  await bookTripMock();
});

afterEach(async () => {
  await mongoose.connection.dropDatabase();
  await mongoose.connection.close();
});

describe("View Travels Booking", () => {
  it("with invalid travels id", async () => {
    await viewTravelsBookingService(
      date,
      "64ab9cde2fa614682c011b24",
      travels.details.role
    ).catch((err) => {
      expect(err.message).toBe(INVALID_TRIP_ID.message);
      expect(err.statusCode).toBe(INVALID_TRIP_ID.status);
    });
  });

  it("with valid trip id", async () => {
    const res = await viewTravelsBookingService(
      date,
      trip_id.toString(),
      travels.details.role
    );

    expect(res).toHaveProperty("bookings");
    expect(res).toHaveProperty("available_seats");
    expect(res).toHaveProperty("seat_cap");
  });
});
