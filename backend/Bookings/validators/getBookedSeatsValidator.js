import Joi from "joi";

const getBookedSeatsValidator = Joi.object().keys({
  trip_id: Joi.string().required().messages({
    "string.empty": `Trip ID cannot be empty`,
    "any.required": `Trip ID is required`,
  }),
  date: Joi.string().required().messages({
    "string.empty": `Date cannot be empty`,
    "any.required": `Date is required`,
  }),
});

export default getBookedSeatsValidator;
