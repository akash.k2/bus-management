import Joi from "joi";

const viewUserBookingValidator = Joi.object().keys({
  user_id: Joi.string().required().messages({
    "string.empty": `User ID cannot be empty`,
    "any.required": `User ID is required`,
  }),
});

export default viewUserBookingValidator;
