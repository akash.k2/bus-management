import Joi from "joi";

const cancelBookingValidator = Joi.object().keys({
  booking_id: Joi.string().required().messages({
    "string.empty": `Booking ID cannot be empty`,
    "any.required": `Booking ID is required`,
  }),
});

export default cancelBookingValidator;
