import Joi from "joi";

const viewTravelsBookingValidator = Joi.object().keys({
  date: Joi.string().required().messages({
    "string.empty": `Date cannot be empty`,
    "any.required": `Date is required`,
  }),
  trip_id: Joi.string().required().messages({
    "string.empty": `Trip ID cannot be empty`,
    "any.required": `Trip ID is required`,
  }),
});

export default viewTravelsBookingValidator;
