import Joi from "joi";

const ticketDetailsValidator = Joi.object().keys({
  user_id: Joi.string().required().messages({
    "string.empty": `Email cannot be empty`,
    "any.required": `Email is required`,
  }),
  date: Joi.string().required().messages({
    "string.empty": `Date cannot be empty`,
    "any.required": `Date is required`,
  }),
  trip_id: Joi.string().required().messages({
    "string.empty": `Trip id cannot be empty`,
    "any.required": `Trip id is required`,
  }),
  passenger_details: Joi.array()
    .items(Joi.object().required())
    .required()
    .messages({
      "array.empty": `Passenger details cannot be empty`,
      "any.required": `Passenger details is required`,
    }),
  seatNumbers: Joi.array()
    .length(Joi.ref("passenger_details.length"))
    .items(Joi.number().required())
    .required()
    .messages({
      "array.empty": `Seat Numbers cannot be empty`,
      "any.required": `Seat Numbers is required`,
      "array.length": "Seat Numbers must match passengers count",
    }),
  travels_name: Joi.string().required().messages({
    "string.empty": `Travels name cannot be empty`,
    "any.required": `Travels name is required`,
  }),
  total_price: Joi.number().required().min(20).messages({
    "number.empty": `Total price cannot be empty`,
    "any.required": `Total price is required`,
    "number.min": "Minimum price is 20",
  }),
});

export default ticketDetailsValidator;
