import CustomError from "../../Utils/CustomError.js";
import asyncErrorHandler from "../../Utils/asyncErrorHandler.js";
import { FETCH_SUCCESS } from "../../Utils/constants.js";

import getUserBookings from "../db/getUserBookings.js";
import viewUserBookingService from "../services/viewUserBookingsService.js";
import viewUserBookingValidator from "../validators/viewUserBookingValidator.js";

const viewUserBookingsController = asyncErrorHandler(async (req, res, next) => {
  const { user_id } = req.params;

  const result = await viewUserBookingService(
    req.user.id,
    user_id,
    req.user.role
  );

  return res.status(FETCH_SUCCESS.status).send({
    prevBookings: result.prevBookings,
    currBookings: result.currBookings,
    cancelledBookings: result.cancelledBookings,
  });
});

export default viewUserBookingsController;
