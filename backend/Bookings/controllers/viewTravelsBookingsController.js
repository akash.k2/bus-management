import asyncErrorHandler from "../../Utils/asyncErrorHandler.js";
import { FETCH_SUCCESS, NO_CONTENT } from "../../Utils/constants.js";

import viewTravelsBookingService from "../services/viewTravelsBookingsService.js";

const viewTravelsBookingsController = asyncErrorHandler(async (req, res) => {
  const { date, trip_id } = req.query;

  const result = await viewTravelsBookingService(date, trip_id, req.user.role);

  return res.status(FETCH_SUCCESS.status).send({
    bookings: result.bookings,
    available_seats: result.available_seats,
    seat_cap: result.seat_cap,
    canceled_bookings: result.canceled_bookings,
  });
});

export default viewTravelsBookingsController;
