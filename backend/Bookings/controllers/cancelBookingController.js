import asyncErrorHandler from "../../Utils/asyncErrorHandler.js";
import { CANCEL_BOOKING_SUCCESS } from "../../Utils/constants.js";

import cancelBookingService from "../services/cancelBookingService.js";

const cancelBookingController = asyncErrorHandler(async (req, res, next) => {
  const { booking_id } = req.body;

  await cancelBookingService(booking_id, req.user.role);

  return res
    .status(CANCEL_BOOKING_SUCCESS.status)
    .send({ message: CANCEL_BOOKING_SUCCESS.message });
});

export default cancelBookingController;
