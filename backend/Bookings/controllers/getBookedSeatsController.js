import getBookedSeatsService from "../services/getBookedSeatsService.js";

import asyncErrorHandler from "../../Utils/asyncErrorHandler.js";
import { FETCH_SUCCESS } from "../../Utils/constants.js";

const getBookedSeatsController = asyncErrorHandler(async (req, res) => {
  const { trip_id, date } = req.query;

  const bookedSeats = await getBookedSeatsService(trip_id, date, req.user.role);

  return res.status(FETCH_SUCCESS.status).send({ bookedSeats });
});

export default getBookedSeatsController;
