import asyncErrorHandler from "../../Utils/asyncErrorHandler.js";
import { BOOKING_SUCCESS } from "../../Utils/constants.js";

import bookTripService from "../services/bookTripService.js";

const bookTripController = asyncErrorHandler(async (req, res, next) => {
  const {
    user_id,
    date,
    trip_id,
    passenger_details,
    seatNumbers,
    travels_name,
    total_price,
  } = req.body;

  const booking = await bookTripService(
    user_id,
    date,
    trip_id,
    passenger_details,
    seatNumbers,
    travels_name,
    total_price,
    req.user.role
  );

  return res
    .status(BOOKING_SUCCESS.status)
    .send({ booking, message: BOOKING_SUCCESS.message });
});

export default bookTripController;
