import { Schema, model } from "mongoose";

const checkDate = (date) => {
  return new Date(date).setHours(0, 0, 0, 0) >= new Date().setHours(0, 0, 0, 0);
};

const bookingSchema = new Schema({
  user_id: {
    type: Schema.Types.ObjectId,
    ref: "User",
    required: [true, "Enter valid user id"],
  },
  date: {
    type: Date,
    validate: [checkDate, "Enter valid travel date"],
    required: [true, "Enter travel date"],
  },
  trip_id: {
    type: Schema.Types.ObjectId,
    ref: "Trip",
    required: [true, "Enter valid trip id"],
  },
  booking_date: {
    type: Date,
    default: Date.now(),
  },
  passenger_details: {
    type: [Object],
    required: [true, "Enter passenger details"],
  },
  seatNumbers: {
    type: [Number],
    required: [true, "Select seat number"],
  },
  user_details: {
    type: Object,
    required: [true, "Enter user details"],
  },
  travels_name: {
    type: String,
    required: [true, "Enter travels name"],
  },
  total_price: {
    type: Number,
    required: [true, "Enter total price"],
    min: [20, "Minimum price is 20"],
  },
  isCancelled: {
    type: Boolean,
    default: false,
  },
});

const Booking = model("Bookings", bookingSchema);

export default Booking;
