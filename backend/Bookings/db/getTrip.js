import Trip from "../../Trips/models/tripModel.js";

import CustomError from "../../Utils/CustomError.js";
import { INVALID_TRIP_ID } from "../../Utils/constants.js";

const getTrip = async (trip_id) => {
  const trip = await Trip.find({ _id: trip_id }, { _id: 1, seat_cap: 1 });

  if (trip.length !== 1) {
    throw new CustomError(INVALID_TRIP_ID.message, INVALID_TRIP_ID.status);
  } else {
    return trip[0].seat_cap;
  }
};

export default getTrip;
