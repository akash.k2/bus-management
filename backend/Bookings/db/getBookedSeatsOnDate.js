import Booking from "../models/bookingModel.js";

const getBookedSeatsOnDate = async (trip_id, date) => {
  const bookedSeats = [];

  const bookings = await Booking.find({ trip_id, date, isCancelled: false });

  bookings.forEach((booking) => {
    const bookingSeats = booking.seatNumbers;

    bookedSeats.push(...bookingSeats);
  });

  return bookedSeats;
};

export default getBookedSeatsOnDate;
