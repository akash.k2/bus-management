import Booking from "../models/bookingModel.js";

import CustomError from "../../Utils/CustomError.js";
import { SEAT_ALREADY_BOOKED } from "../../Utils/constants.js";

const getBookedSeats = async (date, trip_id, seatNumbers) => {
  const all_bookings = await Booking.find({
    date: new Date(date).setHours(0, 0, 0, 0),
    trip_id,
    seatNumbers: { $in: seatNumbers },
  });

  const bookings = all_bookings.filter((booking) => {
    return booking.isCancelled !== true;
  });

  if (bookings.length !== 0) {
    throw new CustomError(
      SEAT_ALREADY_BOOKED.message,
      SEAT_ALREADY_BOOKED.status
    );
  }
};

export default getBookedSeats;
