import Booking from "../models/bookingModel.js";
import Trip from "../../Trips/models/tripModel.js";

const getTravelsBookings = async (date, trip_id, seat_cap) => {
  const selected_date = new Date(date).setHours(0, 0, 0, 0);

  //fetches all bookngs on that date
  const bookings = await Booking.find(
    { trip_id, isCancelled: false, date: selected_date },
    { travels_name: 0 }
  );

  // fetches all cancelled bookings
  const canceled_bookings = await Booking.find(
    {
      trip_id,
      isCancelled: true,
      date: selected_date,
    },
    { travels_name: 0 }
  );

  let bookedCount = 0;

  bookings.forEach((booking) => {
    bookedCount += booking.passenger_details.length;
  });

  const available_seats = seat_cap - bookedCount;

  return {
    bookings,
    available_seats,
    canceled_bookings,
  };
};

export default getTravelsBookings;
