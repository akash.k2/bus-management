import { ObjectId } from "mongodb";
import mongoose from "mongoose";
import Booking from "../models/bookingModel.js";
import getUserInfo from "./getUserInfo.js";

const saveBooking = async (
  user_id,
  date,
  trip_id,
  passDetails,
  seatNumbers,
  travels_name,
  total_price
) => {
  //gets the user's details like email and phone number
  const user_details = await getUserInfo(user_id);

  //saves booking
  const booking = new Booking({
    user_id,
    date: new Date(date).setHours(0, 0, 0, 0),
    trip_id: new ObjectId(trip_id),
    passenger_details: passDetails,
    seatNumbers,
    user_details,
    travels_name,
    total_price,
  });

  await booking.save();

  return booking;

  // await booking.save({ session });
  // await session.commitTransaction();
  // await session.endSession();
};

export default saveBooking;
