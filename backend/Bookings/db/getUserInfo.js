import User from "../../Auth/models/userModel.js";

const getUserInfo = async (user_id) => {
  const userInfo = await User.find(
    { _id: user_id },
    { email: 1, phone: 1, _id: 0 }
  );

  return userInfo[0];
};
export default getUserInfo;
