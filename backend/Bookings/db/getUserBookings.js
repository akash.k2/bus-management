import Booking from "../models/bookingModel.js";
import { ObjectId } from "mongodb";

const viewUserBookings = async (user_id) => {
  const date = new Date();

  const prevBookings = await Booking.aggregate([
    {
      $match: {
        user_id: new ObjectId(user_id),
        date: { $lt: date },
        isCancelled: false,
      },
    },
    {
      $lookup: {
        from: "trips",
        localField: "trip_id",
        foreignField: "_id",
        as: "TripDetails",
      },
    },
    {
      $project: {
        user_id: 0,
        user_details: 0,
      },
    },
    { $sort: { date: -1 } },
  ]);

  const currBookings = await Booking.aggregate([
    {
      $match: {
        user_id: new ObjectId(user_id),
        date: { $gte: date },
        isCancelled: false,
      },
    },
    {
      $lookup: {
        from: "trips",
        localField: "trip_id",
        foreignField: "_id",
        as: "TripDetails",
      },
    },
    {
      $project: {
        user_id: 0,
        user_details: 0,
      },
    },
    { $sort: { date: -1 } },
  ]);

  const cancelledBookings = await Booking.aggregate([
    {
      $match: { user_id: new ObjectId(user_id), isCancelled: true },
    },
    {
      $lookup: {
        from: "trips",
        localField: "trip_id",
        foreignField: "_id",
        as: "TripDetails",
      },
    },
    {
      $project: {
        user_id: 0,
        user_details: 0,
      },
    },
    { $sort: { date: -1 } },
  ]);

  return {
    prevBookings,
    currBookings,
    cancelledBookings,
  };
};

export default viewUserBookings;
