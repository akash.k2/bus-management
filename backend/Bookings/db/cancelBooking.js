import Booking from "../models/bookingModel.js";

import CustomError from "../../Utils/CustomError.js";
import { INVALID_BOOKING_ID } from "../../Utils/constants.js";

const cancelBooking = async (booking_id) => {
  const booking = await Booking.find(
    { _id: booking_id, isCancelled: false },
    { _id: 1 }
  );

  if (booking.length !== 1) {
    throw new CustomError(
      INVALID_BOOKING_ID.message,
      INVALID_BOOKING_ID.status
    );
  } else {
    await Booking.updateOne(
      { _id: booking_id },
      { $set: { isCancelled: true } },
      { runValidators: true }
    );
  }
};

export default cancelBooking;
