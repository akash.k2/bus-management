import Booking from "../models/bookingModel.js";

const getAvailableSeats = async (trip_id, date, seat_cap) => {
  const temp_bookings = await Booking.find(
    {
      trip_id,
      date: new Date(date).setHours(0, 0, 0, 0),
      isCancelled: false,
    },
    { seatNumbers: 1 }
  );

  let bookedSeats = 0;

  for (let j = 0; j < temp_bookings.length; j++) {
    bookedSeats += temp_bookings[j].seatNumbers.length;
  }

  return seat_cap - bookedSeats;
};

export default getAvailableSeats;
