import CustomError from "../../Utils/CustomError.js";
import { INVALID_DATE } from "../../Utils/constants.js";

const checkDate = (date) => {
  if (new Date(date).setHours(0, 0, 0, 0) < new Date().setHours(0, 0, 0, 0)) {
    throw new CustomError(INVALID_DATE.message, INVALID_DATE.status);
  } else {
    return true;
  }
};
export default checkDate;
