const seatMapper = (passenger_details, seatNumbers) => {
  const passDetails = seatNumbers.map((seat, idx) => ({
    seatNo: seat,
    name: passenger_details[idx]?.name,
    age: passenger_details[idx]?.age,
  }));

  return passDetails;
};

export default seatMapper;
