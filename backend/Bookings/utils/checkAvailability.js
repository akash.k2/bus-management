import CustomError from "../../Utils/CustomError.js";
import { SEATS_COUNT_UNAVAILABLE } from "../../Utils/constants.js";

const checkAvailability = (available, required) => {
  if (available < required)
    throw new CustomError(
      SEATS_COUNT_UNAVAILABLE.message,
      SEATS_COUNT_UNAVAILABLE.status
    );
};

export default checkAvailability;
