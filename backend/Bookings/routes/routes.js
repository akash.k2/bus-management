import { Router } from "express";
import verifyToken from "../../middlewares/verifyToken.js";

import adminViewBookingsController from "../controllers/viewTravelsBookingsController.js";
import bookTripController from "../controllers/bookTripController.js";
import userViewBookingsController from "../controllers/viewUserBookingsController.js";
import cancelBookingController from "../controllers/cancelBookingController.js";
import getBookedSeatsController from "../controllers/getBookedSeatsController.js";

const bookingRouter = Router();

bookingRouter.get(
  "/admin/view-bookings",
  verifyToken,
  adminViewBookingsController
);

bookingRouter.get(
  "/view-bookings/:user_id",
  verifyToken,
  userViewBookingsController
);

bookingRouter.get("/get-bookedseats", verifyToken, getBookedSeatsController);

bookingRouter.post("/book-trip", verifyToken, bookTripController);

bookingRouter.post("/cancel-booking", verifyToken, cancelBookingController);

export default bookingRouter;
