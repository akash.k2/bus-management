import User from "../models/userModel.js";

const createUser = async (name, email, hashedPassword, phone) => {
  const user = new User({
    name: name,
    email: email.toLowerCase(),
    password: hashedPassword,
    phone: phone,
  });

  const newUser = await user.save();

  return newUser;
};

export default createUser;
