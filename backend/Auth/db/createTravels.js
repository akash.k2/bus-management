import Travels from "../models/travelsModel.js";

const createTravels = async (name, email, hashedPassword, phone) => {
  const travels = new Travels({
    name: name,
    email: email.toLowerCase(),
    password: hashedPassword,
    phone: phone,
  });

  const newTravels = await travels.save();

  return newTravels;
};

export default createTravels;
