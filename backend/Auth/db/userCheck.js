import CustomError from "../../Utils/CustomError.js";
import { UNAUTHORIZED } from "../../Utils/constants.js";
import User from "../models/userModel.js";

const userCheck = async (email) => {
  const user = await User.find(
    { email: email },
    { name: 1, _id: 1, password: 1 }
  );

  if (user.length !== 1) {
    throw new CustomError(UNAUTHORIZED.message, UNAUTHORIZED.status);
  } else {
    return user;
  }
};

export default userCheck;
