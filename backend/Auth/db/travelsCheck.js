import { UNAUTHORIZED } from "../../Utils/constants.js";
import Travels from "../models/travelsModel.js";
import CustomError from "../../Utils/CustomError.js";

const TravelsCheck = async (email) => {
  const travels = await Travels.find(
    { email: email },
    { name: 1, _id: 1, password: 1 }
  );

  if (travels.length !== 1) {
    throw new CustomError(UNAUTHORIZED.message, UNAUTHORIZED.status);
  } else {
    return travels;
  }
};

export default TravelsCheck;
