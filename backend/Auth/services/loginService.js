import userCheck from "../db/userCheck.js";
import TravelsCheck from "../db/travelsCheck.js";
import generateToken from "../services/generateToken.js";

import bycrypt from "../utils/bcrypt.js";

import globalValidator from "../../Utils/globalValidator.js";
import loginValidator from "../validators/loginValidator.js";

const loginService = async (email, password, role) => {
  // validates the input from client
  if (globalValidator(loginValidator, { email, password, role })) {
    let user;

    // checks if there is an email in the db
    if (role === "user") {
      user = await userCheck(email);
    } else if (role === "travels") {
      user = await TravelsCheck(email);
    }

    if (user) {
      // converts the given password and checks it with the db
      const isValid = await bycrypt.comparePassword(password, user[0].password);

      if (isValid === "Success") {
        const token = generateToken(user[0].name, user[0]._id, role);

        const details = {
          name: user[0].name,
          id: user[0]._id,
          role: role,
        };

        return {
          token,
          details,
        };
      }
    }
  }
};

export default loginService;
