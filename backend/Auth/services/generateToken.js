import jwt from "jsonwebtoken";

const generateToken = (name, id, role) => {
  const token = jwt.sign(
    { name: name, id: id, role: role },
    process.env.TOKEN_SECRET.toString(),
    { expiresIn: "43200s" }
  );

  return token;
};

export default generateToken;
