import bycrypt from "../utils/bcrypt.js";
import generateToken from "../services/generateToken.js";
import createUser from "../db/createUser.js";
import createTravels from "../db/createTravels.js";

import globalValidator from "../../Utils/globalValidator.js";
import registerValidator from "../validators/registerValidator.js";

const registerService = async (name, email, password, role, phone) => {
  //validates the input from client
  if (
    globalValidator(registerValidator, { name, email, password, role, phone })
  ) {
    const hashedPassword = await bycrypt.hashPassword(password);

    let newUser;

    // creates the user
    if (role === "user") {
      newUser = await createUser(name, email, hashedPassword, phone);
    } else if (role === "travels") {
      newUser = await createTravels(name, email, hashedPassword, phone);
    }

    //generate jwt token and sign it
    const token = generateToken(name, newUser._id, role);

    const details = {
      name: newUser.name,
      id: newUser._id,
      role: role,
    };

    return {
      token,
      details,
    };
  }
};

export default registerService;
