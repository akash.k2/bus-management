import { FETCH_SUCCESS, FORBIDDEN } from "../../Utils/constants.js";

const getUserDetails = (req, res) => {
  if (req.user) {
    const details = {
      name: req.user.name,
      id: req.user.id,
      role: req.user.role,
    };

    return res.status(FETCH_SUCCESS.status).send({ details });
  } else {
    return res.status(FORBIDDEN.status).send({ message: FORBIDDEN.message });
  }
};

export default getUserDetails;
