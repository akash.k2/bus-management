import asyncErrorHandler from "../../Utils/asyncErrorHandler.js";
import { LOGIN_SUCCESS, REGISTER_SUCCESS } from "../../Utils/constants.js";

import loginService from "../services/loginService.js";
import registerService from "../services/registerService.js";

export const RegisterController = asyncErrorHandler(async (req, res, next) => {
  const { name, email, password, role, phone } = req.body;

  const result = await registerService(name, email, password, role, phone);

  return res.status(REGISTER_SUCCESS.status).send({
    message: REGISTER_SUCCESS.message,
    token: result.token,
    details: result.details,
  });
});

export const LoginController = asyncErrorHandler(async (req, res, next) => {
  const { email, password, role } = req.body;

  const result = await loginService(email, password, role);

  return res.status(LOGIN_SUCCESS.status).send({
    message: LOGIN_SUCCESS.message,
    token: result.token,
    details: result.details,
  });
});
