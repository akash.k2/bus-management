import bcrypt from "bcrypt";
import CustomError from "../../Utils/CustomError.js";
import { UNAUTHORIZED } from "../../Utils/constants.js";

const hashPassword = async (password) => {
  return await bcrypt.hash(password, 10);
};

const comparePassword = async (password, hashedpassword) => {
  try {
    if (!(await bcrypt.compare(password, hashedpassword))) {
      throw new CustomError(UNAUTHORIZED.message, UNAUTHORIZED.status);
    } else {
      return "Success";
    }
  } catch (e) {
    throw new CustomError(UNAUTHORIZED.message, UNAUTHORIZED.status);
  }
};

export default { hashPassword, comparePassword };
