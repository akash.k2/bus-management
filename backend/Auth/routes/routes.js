import { Router } from "express";

import verifyToken from "../../middlewares/verifyToken.js";

import {
  RegisterController,
  LoginController,
} from "../controllers/loginController.js";

import getUserDetails from "../controllers/getUserDetails.js";

const authRouter = Router();

authRouter.get("/get-details", verifyToken, getUserDetails);

authRouter.post("/register", RegisterController);

authRouter.post("/login", LoginController);

export default authRouter;
