import dotenv from "dotenv";
dotenv.config();

import mongoose from "mongoose";
import { MongoMemoryServer } from "mongodb-memory-server";

import registerService from "../services/registerService.js";

import { userRegisterMock } from "../../Utils/mocks.js";

beforeEach(async () => {
  const mongoServer = await MongoMemoryServer.create();
  const url = mongoServer.getUri();
  await mongoose.connect(url);
  await userRegisterMock();
});

afterEach(async () => {
  await mongoose.connection.dropDatabase();
  await mongoose.connection.close();
});

describe("Register", () => {
  it("register new user", async () => {
    const res = await registerService(
      "test-user",
      "testuser1@gmail.com",
      "Test12345$",
      "user",
      "9988776655"
    );

    expect(res).toHaveProperty("token");
    expect(res).toHaveProperty("details");
    expect(res.details.name).toBe("test-user");
    expect(res.details.role).toBe("user");
    expect(res.details).toHaveProperty("id");
  });

  it("register with already existing token", async () => {
    await registerService(
      "test-user",
      "testuser@gmail.com",
      "Test12345$",
      "user",
      "9988776655"
    ).catch((err) => {
      expect(err.message).toMatch("E11000 duplicate key error collection");
      expect(err.code).toBe(11000);
    });
  });
});
