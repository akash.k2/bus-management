import dotenv from "dotenv";
dotenv.config();

import mongoose from "mongoose";
import { MongoMemoryServer } from "mongodb-memory-server";

import loginService from "../services/loginService.js";
import { UNAUTHORIZED } from "../../Utils/constants.js";
import { userRegisterMock } from "../../Utils/mocks.js";

beforeEach(async () => {
  const mongoServer = await MongoMemoryServer.create();
  const url = mongoServer.getUri();
  await mongoose.connect(url);
  await userRegisterMock();
});

afterEach(async () => {
  await mongoose.connection.dropDatabase();
  await mongoose.connection.close();
});

describe("Login", () => {
  it("with wrong credentials", async () => {
    const userData = {
      email: "testuser@gmail.com",
      password: "Test12345",
      role: "user",
    };

    await loginService(userData.email, userData.password, userData.role).catch(
      (err) => {
        expect(err.message).toBe(UNAUTHORIZED.message);
        expect(err.statusCode).toBe(UNAUTHORIZED.status);
      }
    );
  });

  it("with correct credentials", async () => {
    const userData = {
      email: "testuser@gmail.com",
      password: "Test12345$",
      role: "user",
    };

    const res = await loginService(
      userData.email,
      userData.password,
      userData.role
    );

    expect(res).toHaveProperty("token");
    expect(res).toHaveProperty("details");
    expect(res.details.name).toBe("test-user");
    expect(res.details.role).toBe("user");
    expect(res.details).toHaveProperty("id");
  });
});
