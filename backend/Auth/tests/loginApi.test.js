import { config } from "dotenv";
config();

import mongoose from "mongoose";
import request from "supertest";

import app from "../../app.js";
import { LOGIN_SUCCESS, UNAUTHORIZED } from "../../Utils/constants.js";

beforeEach(async () => {
  await mongoose.disconnect();
  await mongoose.connect(process.env.MONGO_URL);
});

afterEach(async () => {
  await mongoose.disconnect();
  await mongoose.connection.close();
});

describe("Login", () => {
  it("with correct credentials", async () => {
    const userData = {
      email: "akashkumarms03@gmail.com",
      password: "Akash123$",
      role: "user",
    };

    const res = await request(app).post("/login").send(userData);

    expect(res.statusCode).toBe(200);
    expect(JSON.parse(res.text).message).toEqual(LOGIN_SUCCESS.message);
    expect(JSON.parse(res.text)).toHaveProperty("token");
    expect(JSON.parse(res.text).details.role).toEqual(userData.role);
  });

  // it("with incorrect credentials", async () => {
  //   const userData = {
  //     email: "akashkumarms03@gmail.com",
  //     password: "akash123$",
  //     role: "user",
  //   };
  //   await request(app)
  //     .post("/login")
  //     .send(userData)
  //     .catch((err) => {
  //       console.log(err);
  //       expect(err.message).toBe(UNAUTHORIZED.message);
  //       expect(err.statusCode).toBe(UNAUTHORIZED.status);
  //     });
  // });
});
