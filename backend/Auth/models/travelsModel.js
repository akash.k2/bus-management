import { Schema, model } from "mongoose";

const validatePhone = (phone) => {
  return phone.toString().length === 10;
};

const travelsSchema = new Schema({
  name: {
    type: String,
    required: [true, "Enter name"],
  },
  phone: {
    type: Number,
    trim: true,
    required: [true, "Enter phone number"],
    validate: [validatePhone, "Enter a valid phone number"],
  },
  email: {
    type: String,
    unique: true,
    trim: true,
    lowercase: true,
    required: [true, "Enter email address"],
    match: [
      /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,
      "Enter a valid email address",
    ],
  },
  password: {
    type: String,
    required: [true, "Enter password"],
  },
  isAuthenticated: {
    type: Boolean,
    default: false,
  },
});

const Travels = model("Travels", travelsSchema);
export default Travels;
