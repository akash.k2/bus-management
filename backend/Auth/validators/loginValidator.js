import Joi from "joi";

const loginValidator = Joi.object().keys({
  email: Joi.string().required().messages({
    "string.empty": `Email cannot be empty`,
    "any.required": `Email is required`,
  }),
  password: Joi.string().required().messages({
    "string.empty": `Password cannot be empty`,
    "any.required": `Password is required`,
  }),
  role: Joi.string().valid("user", "travels").required(),
});

export default loginValidator;
