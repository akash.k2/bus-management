import Joi from "joi";

const registerValidator = Joi.object().keys({
  name: Joi.string().required().messages({
    "string.empty": `name cannot be an empty field`,
    "any.required": `Name is required`,
  }),
  phone: Joi.string()
    .regex(/^[0-9]{10}$/)
    .required()
    .messages({ "string.pattern.base": `Phone number must be 10 digits.` }),
  email: Joi.string()
    .email({
      minDomainSegments: 2,
      tlds: { allow: ["com", "net", "co", "org", "in"] },
    })
    .required(),
  password: Joi.string()
    .min(8)
    .regex(/^(?=.*\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z]).{8,}$/)
    .required()
    .messages({
      "string.min":
        "Password must be minimum 8 characters, with at least a symbol, upper and lower case letters and a number",
      "object.regex": `Password must be minimum 8 characters, with at least a symbol, upper and lower case letters and a number`,
      "string.pattern.base":
        "Password must be minimum 8 characters, with at least a symbol, upper and lower case letters and a number",
      "string.empty": `Password cannot be empty`,
      "any.required": `Password is required`,
    }),
  role: Joi.string().valid("user", "travels").required(),
});

export default registerValidator;
