import jwt from "jsonwebtoken";
import { FORBIDDEN } from "../Utils/constants.js";

function verifyToken(req, res, next) {
  const authHeader = req.headers["authorization"];

  const bearer = authHeader && authHeader.split(" ")[0];
  const token = authHeader && authHeader.split(" ")[1];

  if (bearer !== "Bearer" || token == null)
    return res.status(FORBIDDEN.status).send({ message: FORBIDDEN.message });

  try {
    const decoded = jwt.verify(token, process.env.TOKEN_SECRET.toString());
    req.user = decoded;
    next();
  } catch (error) {
    res.status(FORBIDDEN.status).send({ message: FORBIDDEN.message });
  }
}

export default verifyToken;
