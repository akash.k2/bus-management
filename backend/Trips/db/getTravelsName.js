import Travels from "../../Auth/models/travelsModel.js";

const getTravelsName = async (travels_id) => {
  const travels_name = await Travels.find(
    { _id: travels_id.toString() },
    { name: 1, _id: 0 }
  );
  return travels_name[0].name;
};
export default getTravelsName;
