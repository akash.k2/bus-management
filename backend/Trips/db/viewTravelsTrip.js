import { ObjectId } from "mongodb";
import Trip from "../models/tripModel.js";

const viewTravelsTrip = async (travels_id, day) => {
  const trips = await Trip.find({
    travels_id: new ObjectId(travels_id),
    available_days: { $in: [day] },
  });

  if (trips.length !== 0) {
    return trips;
  } else {
    return "No trips";
  }
};

export default viewTravelsTrip;
