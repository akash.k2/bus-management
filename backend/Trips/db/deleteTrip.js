import Trip from "../models/tripModel.js";

const deleteTrip = async (trip_id) => {
  await Trip.deleteOne({ _id: trip_id });
};

export default deleteTrip;
