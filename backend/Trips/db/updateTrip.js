import Trip from "../models/tripModel.js";

const updateTrip = async (
  trip_id,
  src,
  dest,
  start_time,
  duration,
  seat_cap,
  available_days,
  price
) => {
  const updatedTrip = {
    src,
    dest,
    start_time,
    duration,
    seat_cap,
    available_days,
    price,
  };

  await Trip.updateOne(
    { _id: trip_id },
    { $set: updatedTrip },
    { runValidators: true }
  );

  return updatedTrip;
};
export default updateTrip;
