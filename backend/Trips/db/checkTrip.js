import Trip from "../models/tripModel.js";
import CustomError from "../../Utils/CustomError.js";
import { INVALID_TRIP_ID } from "../../Utils/constants.js";

const checkTrip = async (trip_id) => {
  const trip = await Trip.find({ _id: trip_id }, { _id: 1 });

  if (trip.length !== 1) {
    throw new CustomError(INVALID_TRIP_ID.message, INVALID_TRIP_ID.status);
  } else {
    return true;
  }
};

export default checkTrip;
