import { ObjectId } from "mongodb";
import Trip from "../models/tripModel.js";

import getTravelsName from "./getTravelsName.js";

const addTrip = async (
  src,
  dest,
  start_time,
  duration,
  seat_cap,
  travels_id,
  available_days,
  price
) => {
  //get Travels Name
  const travels_name = await getTravelsName(travels_id);

  const trip = new Trip({
    src,
    dest,
    start_time,
    duration,
    seat_cap,
    travels_id: new ObjectId(travels_id),
    travels_name,
    available_days,
    price,
  });

  const newTrip = await trip.save();

  return newTrip._id;
};

export default addTrip;
