import Trip from "../models/tripModel.js";
import Booking from "../../Bookings/models/bookingModel.js";

const viewUserTrip = async (src, dest, date) => {
  const trips = await Trip.find({
    src,
    dest,
    available_days: { $in: [new Date(date).getDay()] },
  });

  if (trips.length !== 0) {
    for (let i = 0; i < trips.length; i++) {
      const bookings = await Booking.find({
        trip_id: trips[i]._id,
        date: new Date(date).setHours(0, 0, 0, 0),
        isCancelled: false,
      });

      let bookedSeats = 0;

      for (let j = 0; j < bookings.length; j++) {
        bookedSeats += bookings[j].seatNumbers.length;
      }

      trips[i].available_seats = trips[i].seat_cap - bookedSeats;
    }
    return trips;
  } else {
    return "No trips";
  }
};

export default viewUserTrip;
