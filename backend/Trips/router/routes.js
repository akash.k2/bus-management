import { Router } from "express";
import verifyToken from "../../middlewares/verifyToken.js";

import addTripController from "../controllers/addTripController.js";
import adminViewTripsController from "../controllers/viewTravelsTripsController.js";
import updateTripController from "../controllers/updateTripController.js";
import deleteTripController from "../controllers/deleteTripController.js";
import userViewTripsController from "../controllers/viewUserTripsController.js";

const tripRouter = Router();

tripRouter.get("/view-trips", verifyToken, userViewTripsController);

tripRouter.get(
  "/admin/view-trips/:travels_id",
  verifyToken,
  adminViewTripsController
);

tripRouter.post("/admin/add-trip", verifyToken, addTripController);

tripRouter.patch("/admin/update-trip", verifyToken, updateTripController);

tripRouter.delete("/admin/delete-trip", verifyToken, deleteTripController);

export default tripRouter;
