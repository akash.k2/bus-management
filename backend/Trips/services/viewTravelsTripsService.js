import viewTravelsTrip from "../db/viewTravelsTrip.js";

import roleValidator from "../../Utils/roleValidator.js";
import CustomError from "../../Utils/CustomError.js";
import globalValidator from "../../Utils/globalValidator.js";

import viewTravelsTripValidator from "../validators/viewTravelsTripValidator.js";

import { INVALID_TRAVELS_ID, TRAVELS } from "../../Utils/constants.js";

const viewTravelsTripsService = async (
  token_id,
  travels_id,
  day,
  token_role
) => {
  if (roleValidator(token_role, TRAVELS)) {
    if (token_id !== travels_id) {
      throw new CustomError(
        INVALID_TRAVELS_ID.message,
        INVALID_TRAVELS_ID.status
      );
    } else {
      if (globalValidator(viewTravelsTripValidator, { travels_id, day })) {
        //fetches all the trips based on available days
        const trips = await viewTravelsTrip(travels_id, day);

        return trips;
      }
    }
  }
};

export default viewTravelsTripsService;
