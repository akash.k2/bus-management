import checkTrip from "../db/checkTrip.js";
import updateTrip from "../db/updateTrip.js";

import globalValidator from "../../Utils/globalValidator.js";
import updateTripValidator from "../validators/updateTripValidator.js";
import roleValidator from "../../Utils/roleValidator.js";
import { TRAVELS } from "../../Utils/constants.js";

const updateTripService = async (
  src,
  dest,
  start_time,
  duration,
  seat_cap,
  available_days,
  price,
  trip_id,
  token_role
) => {
  if (
    roleValidator(token_role, TRAVELS) &&
    globalValidator(updateTripValidator, {
      src,
      dest,
      start_time,
      duration,
      seat_cap,
      available_days,
      price,
      trip_id,
    })
  ) {
    //checks the validity of the given trip id
    if (await checkTrip(trip_id)) {
      //updates the trip with given values
      const updatedTrip = await updateTrip(
        trip_id,
        src,
        dest,
        start_time,
        duration,
        seat_cap,
        available_days,
        price
      );

      return updatedTrip;
    }
  }
};

export default updateTripService;
