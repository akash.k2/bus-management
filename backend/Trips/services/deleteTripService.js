import checkTrip from "../db/checkTrip.js";
import deleteTrip from "../db/deleteTrip.js";

import roleValidator from "../../Utils/roleValidator.js";
import globalValidator from "../../Utils/globalValidator.js";
import deleteTripValidator from "../validators/deleteTripValidator.js";

import { TRAVELS } from "../../Utils/constants.js";

const deleteTripService = async (trip_id, token_role) => {
  if (
    roleValidator(token_role, TRAVELS) &&
    globalValidator(deleteTripValidator, { data: trip_id })
  ) {
    //checks if it is a valid trip id
    if (await checkTrip(trip_id)) {
      //deletes the trip
      await deleteTrip(trip_id);
    }
    return true;
  }
};

export default deleteTripService;
