import CustomError from "../../Utils/CustomError.js";

import addTrip from "../db/addTrip.js";

import roleValidator from "../../Utils/roleValidator.js";
import globalValidator from "../../Utils/globalValidator.js";
import addTripValidator from "../validators/addTripValidator.js";

import capitalizeWord from "../utils/capitalizeWord.js";
import { INVALID_TRAVELS_ID, TRAVELS } from "../../Utils/constants.js";

const addTripService = async (
  token_id,
  src,
  dest,
  start_time,
  duration,
  seat_cap,
  travels_id,
  available_days,
  price,
  token_role
) => {
  if (roleValidator(token_role, TRAVELS))
    if (token_id !== travels_id) {
      throw new CustomError(
        INVALID_TRAVELS_ID.message,
        INVALID_TRAVELS_ID.status
      );
    } else {
      if (
        globalValidator(addTripValidator, {
          src,
          dest,
          start_time,
          duration,
          seat_cap,
          travels_id,
          available_days,
          price,
        })
      ) {
        //converts the given value to desired format
        const source = capitalizeWord(src.trim());
        const destination = capitalizeWord(dest.trim());

        //creates a new trip
        const trip_id = await addTrip(
          source,
          destination,
          start_time,
          duration,
          seat_cap,
          travels_id,
          available_days,
          price
        );

        return trip_id;
      }
    }
};

export default addTripService;
