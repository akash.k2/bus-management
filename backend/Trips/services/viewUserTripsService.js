import viewUserTrip from "../db/viewUserTrips.js";

import globalValidator from "../../Utils/globalValidator.js";
import viewUserTripValidator from "../validators/viewUserTripValidator.js";
import roleValidator from "../../Utils/roleValidator.js";
import { USER } from "../../Utils/constants.js";

const viewUserTripsService = async (src, dest, date, token_role) => {
  if (
    roleValidator(token_role, USER) &&
    globalValidator(viewUserTripValidator, { src, dest, date })
  ) {
    //fetches all the trips based on source, destination, and given date
    const trips = await viewUserTrip(src, dest, date);

    return trips;
  }
};

export default viewUserTripsService;
