import dotenv from "dotenv";
dotenv.config();

import mongoose from "mongoose";
import { MongoMemoryServer } from "mongodb-memory-server";

import { addTripMock, travelsRegisterMock } from "../../Utils/mocks.js";

import updateTripService from "../services/updateTripService.js";

import { TRAVELS } from "../../Utils/constants.js";

let travels;
let trip_id;

beforeEach(async () => {
  const mongoServer = await MongoMemoryServer.create();
  const url = mongoServer.getUri();
  await mongoose.connect(url);

  travels = await travelsRegisterMock();
  trip_id = await addTripMock();
});

afterEach(async () => {
  await mongoose.connection.dropDatabase();
  await mongoose.connection.close();
});

describe("update trips", () => {
  it("failed", async () => {
    await updateTripService(
      "Chennai",
      "Madurai",
      "22:00",
      8,
      8,
      [0, 1, 2, 3, 4, 5, 6],
      700,
      trip_id.toString(),
      TRAVELS
    ).catch((err) => {
      expect(err.message).toBe("Minimum seat capacity is 15");
      expect(err.statusCode).toBe(400);
    });
  });

  it("successfully", async () => {
    const updatedTrip = await updateTripService(
      "Chennai",
      "Madurai",
      "22:00",
      8,
      35,
      [0, 1, 2, 3, 4, 5, 6],
      700,
      trip_id.toString(),
      TRAVELS
    );

    expect(updatedTrip.start_time).toBe("22:00");
    expect(updatedTrip.seat_cap).toBe(35);
  });
});
