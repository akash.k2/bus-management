import dotenv from "dotenv";
dotenv.config();

import mongoose from "mongoose";
import { MongoMemoryServer } from "mongodb-memory-server";

import { addTripMock, travelsRegisterMock } from "../../Utils/mocks.js";

import deleteTripService from "../services/deleteTripService.js";

import { INVALID_TRIP_ID, TRAVELS } from "../../Utils/constants.js";

let travels;
let trip_id;

beforeEach(async () => {
  const mongoServer = await MongoMemoryServer.create();
  const url = mongoServer.getUri();
  await mongoose.connect(url);

  travels = await travelsRegisterMock();
  trip_id = await addTripMock();
});

afterEach(async () => {
  await mongoose.connection.dropDatabase();
  await mongoose.connection.close();
});

describe("deletes trip", () => {
  it("successfully", async () => {
    const deleteTrip = await deleteTripService(trip_id.toString(), TRAVELS);

    expect(deleteTrip).toBe(true);
  });
});

describe("deletes trip", () => {
  it("with invalid trip id", async () => {
    await deleteTripService(trip_id.toString(), TRAVELS).catch((err) => {
      expect(err.message).toBe(INVALID_TRIP_ID.message);
      expect(err.statusCode).toBe(INVALID_TRIP_ID.status);
    });
  });
});
