import dotenv from "dotenv";
dotenv.config();

import mongoose from "mongoose";
import { MongoMemoryServer } from "mongodb-memory-server";

import { travelsRegisterMock } from "../../Utils/mocks.js";
import addTripService from "../services/addTripService.js";

import { INVALID_TRAVELS_ID } from "../../Utils/constants";

let travels;

beforeEach(async () => {
  const mongoServer = await MongoMemoryServer.create();
  const url = mongoServer.getUri();
  await mongoose.connect(url);
  travels = await travelsRegisterMock();
});

afterEach(async () => {
  await mongoose.connection.dropDatabase();
  await mongoose.connection.close();
});

describe("Create trip", () => {
  it("with incorrect details", async () => {
    const travels_role = travels.details.role;
    const travels_id = travels.details.id.toString();

    await addTripService(
      travels_id,
      "Chennai",
      "Madurai",
      "23:00",
      8,
      30,
      "64ab9cde2fa614682c011b24",
      [1, 2],
      700,
      travels_role
    ).catch((err) => {
      expect(err.message).toBe(INVALID_TRAVELS_ID.message);
      expect(err.statusCode).toBe(INVALID_TRAVELS_ID.status);
    });
  });

  it("with correct details", async () => {
    const travels_role = travels.details.role;
    const travels_id = travels.details.id.toString();

    const trip_id = await addTripService(
      travels_id,
      "Chennai",
      "Madurai",
      "23:00",
      8,
      30,
      travels_id,
      [0, 1, 2, 3, 4, 5, 6],
      700,
      travels_role
    );

    expect(trip_id).toBeTruthy();
  });
});
