import dotenv from "dotenv";
dotenv.config();

import mongoose from "mongoose";
import { MongoMemoryServer } from "mongodb-memory-server";

import {
  addTripMock,
  travelsRegisterMock,
  userRegisterMock,
} from "../../Utils/mocks.js";

import viewUserTripsService from "../../Trips/services/viewUserTripsService.js";

let user;
beforeEach(async () => {
  const mongoServer = await MongoMemoryServer.create();
  const url = mongoServer.getUri();
  await mongoose.connect(url);
  await travelsRegisterMock();
  await addTripMock();
  user = await userRegisterMock();
});

afterEach(async () => {
  await mongoose.connection.dropDatabase();
  await mongoose.connection.close();
});

describe("View user trips", () => {
  it("trips available", async () => {
    const user_role = user.details.role;

    const trips = await viewUserTripsService(
      "Chennai",
      "Madurai",
      new Date().toISOString(),
      user_role
    );

    expect(trips.length).toBeGreaterThan(0);
  });

  it("trips unavailable", async () => {
    const user_role = user.details.role;

    const trips = await viewUserTripsService(
      "Chennai",
      "Coimbatore",
      new Date().toISOString(),
      user_role
    );

    expect(trips).toBe("No trips");
  });
});
