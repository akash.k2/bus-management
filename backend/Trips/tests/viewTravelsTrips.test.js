import dotenv from "dotenv";
dotenv.config();

import mongoose from "mongoose";
import { MongoMemoryServer } from "mongodb-memory-server";

import { addTripMock, travelsRegisterMock } from "../../Utils/mocks.js";

import viewTravelsTripsService from "../services/viewTravelsTripsService.js";

import { TRAVELS } from "../../Utils/constants.js";

let travels;
let trip_id;

beforeEach(async () => {
  const mongoServer = await MongoMemoryServer.create();
  const url = mongoServer.getUri();
  await mongoose.connect(url);
  travels = await travelsRegisterMock();
  trip_id = await addTripMock();
});

afterEach(async () => {
  await mongoose.connection.dropDatabase();
  await mongoose.connection.close();
});

describe("View travels trips", () => {
  it("trips available", async () => {
    const travels_id = travels.details.id.toString();

    const trips = await viewTravelsTripsService(
      travels_id,
      travels_id,
      1,
      TRAVELS
    );

    expect(trips.length).toBeGreaterThan(0);
  });

  it("trips unavailable", async () => {
    const travels_id = travels.details.id.toString();

    await viewTravelsTripsService(travels_id, travels_id, 7, TRAVELS).catch(
      (err) => {
        expect(err.message).toBe("Enter valid day");
        expect(err.statusCode).toBe(400);
      }
    );
  });
});
