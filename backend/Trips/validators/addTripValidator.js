import Joi from "joi";

const addTripValidator = Joi.object().keys({
  src: Joi.string().required().messages({
    "string.empty": `Source cannot be empty`,
    "any.required": `Source is required`,
  }),
  dest: Joi.string().required().messages({
    "string.empty": `Destination cannot be empty`,
    "any.required": `Destination is required`,
  }),

  start_time: Joi.string()
    .pattern(new RegExp("^(2[0-3]|[01]?[0-9]):([0-5]?[0-9])$"))
    .required()
    .messages({
      "string.base": "Enter valid departure time (hh:mm)",
      "string.pattern.base": "Enter valid departure time (hh:mm)",
      "string.empty": `Departure Time cannot be empty`,
      "any.required": `Departure Time is required`,
    }),
  duration: Joi.number().min(1).max(100).required().messages({
    "number.base": `Enter duration in correct format`,
    "number.empty": `Duration cannot be empty`,
    "number.min": "Minimum duration is 1 hour",
    "number.max": "Maximum duration is 100 hours",
    "any.required": `Duration is required`,
  }),
  travels_id: Joi.string().required().messages({
    "string.empty": `Travels id cannot be empty`,
    "any.required": `Travels id is required`,
  }),
  seat_cap: Joi.number().required().min(15).max(120).messages({
    "number.empty": `Seat capacity cannot be empty`,
    "any.required": `Seat capacity is required`,
    "number.max": "Maximum seat capacity is 120",
    "number.min": "Minimum seat capacity is 15",
  }),
  available_days: Joi.array()
    .min(1)
    .max(7)
    .items(Joi.number().max(6).min(0))
    .required()
    .messages({
      "array.min": "Trip should be atleast a day",
      "array.empty": `Available days cannot be empty`,
      "array.required": `Available days is required`,
      "array.max": "Trip can run only seven times in a week",
      "number.min": "Enter valid day",
      "number.max": "Enter valid day",
    }),
  price: Joi.number().required().min(20).messages({
    "number.empty": `Price cannot be empty`,
    "any.required": `Price is required`,
    "number.min": "Minimum price is 20",
  }),
});

export default addTripValidator;
