import Joi from "joi";

const deleteTripValidator = Joi.object().keys({
  data: Joi.string().required().messages({
    "string.empty": `Trip ID cannot be empty`,
    "any.required": `Trip ID is required`,
  }),
});

export default deleteTripValidator;
