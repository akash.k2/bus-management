import Joi from "joi";

const viewUserTripValidator = Joi.object().keys({
  src: Joi.string().required().messages({
    "string.empty": `Source cannot be empty`,
    "any.required": `Source is required`,
  }),
  dest: Joi.string().required().messages({
    "string.empty": `Destination cannot be empty`,
    "any.required": `Destination is required`,
  }),
  date: Joi.string().required().messages({
    "string.empty": `Date cannot be empty`,
    "any.required": `Date is required`,
  }),
});

export default viewUserTripValidator;
