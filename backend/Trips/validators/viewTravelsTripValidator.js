import Joi from "joi";

const viewTravelsTripValidator = Joi.object().keys({
  travels_id: Joi.string().required().messages({
    "string.empty": `Travels ID cannot be empty`,
    "any.required": `Travels ID is required`,
  }),
  day: Joi.number().required().min(0).max(6).messages({
    "number.base": "Day should be given to its equivalent number",
    "number.empty": `Trip Day cannot be empty`,
    "number.min": `Enter valid day`,
    "number.max": `Enter valid day`,
    "any.required": `Trip Day is required`,
  }),
});

export default viewTravelsTripValidator;
