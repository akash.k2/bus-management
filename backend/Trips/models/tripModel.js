import { Schema, model } from "mongoose";

const tripSchema = new Schema({
  src: {
    type: String,
    trim: true,
    required: [true, "Enter source"],
  },
  dest: {
    type: String,
    trim: true,
    required: [true, "Enter destination"],
  },
  start_time: {
    type: String,
    required: [true, "Enter start time"],
  },
  duration: {
    type: Number,
    required: [true, "Enter duration"],
  },
  seat_cap: {
    type: Number,
    min: [15, "Seat capacity sholud be atleast 15"],
    max: [120, "Seat capacity can be atmost 120"],
    required: [true, "Enter seat capaciity"],
  },
  available_seats: {
    type: Number,
    default: 0,
  },
  travels_id: {
    type: Schema.Types.ObjectId,
    ref: "Travels",
    required: [true, "Enter travels id"],
  },
  travels_name: {
    type: String,
    required: [true, "Enter travels name"],
  },
  available_days: {
    type: [Number],
    required: [true, "Enter atleast one day of trip"],
  },
  price: {
    type: Number,
    required: [true, "Enter price"],
    min: [20, "Minimum price is 20"],
  },
});

const Trip = model("Trips", tripSchema);

export default Trip;
