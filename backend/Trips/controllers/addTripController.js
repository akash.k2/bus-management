import asyncErrorHandler from "../../Utils/asyncErrorHandler.js";
import { CREATE_TRIP_SUCCESS } from "../../Utils/constants.js";

import addTripService from "../services/addTripService.js";

const addTripController = asyncErrorHandler(async (req, res, next) => {
  const {
    src,
    dest,
    start_time,
    duration,
    seat_cap,
    travels_id,
    available_days,
    price,
  } = req.body;

  await addTripService(
    req.user.id,
    src,
    dest,
    start_time,
    duration,
    seat_cap,
    travels_id,
    available_days,
    price,
    req.user.role
  );

  return res
    .status(CREATE_TRIP_SUCCESS.status)
    .send({ message: CREATE_TRIP_SUCCESS.message });
});

export default addTripController;
