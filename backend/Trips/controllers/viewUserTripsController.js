import asyncErrorHandler from "../../Utils/asyncErrorHandler.js";

import viewUserTripsService from "../services/viewUserTripsService.js";

import { FETCH_SUCCESS, NO_CONTENT } from "../../Utils/constants.js";

const viewUserTripsController = asyncErrorHandler(async (req, res, next) => {
  const { src, dest, date } = req.query;

  const trips = await viewUserTripsService(src, dest, date, req.user.role);

  if (trips === "No trips") {
    return res.status(NO_CONTENT.status).send({ message: NO_CONTENT.message });
  } else if (trips) {
    return res.status(FETCH_SUCCESS.status).send({ trips });
  }
});

export default viewUserTripsController;
