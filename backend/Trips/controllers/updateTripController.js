import asyncErrorHandler from "../../Utils/asyncErrorHandler.js";
import { UPDATE_TRIP_SUCCESS } from "../../Utils/constants.js";

import updateTripService from "../services/updateTripService.js";

const updateTripController = asyncErrorHandler(async (req, res, next) => {
  const {
    src,
    dest,
    start_time,
    duration,
    seat_cap,
    available_days,
    price,
    trip_id,
  } = req.body;

  const updatedTrip = await updateTripService(
    src,
    dest,
    start_time,
    duration,
    seat_cap,
    available_days,
    price,
    trip_id,
    req.user.role
  );

  res.status(UPDATE_TRIP_SUCCESS.status).send({
    message: UPDATE_TRIP_SUCCESS.message,
    updatedTrip,
  });
});

export default updateTripController;
