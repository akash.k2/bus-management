import asyncErrorHandler from "../../Utils/asyncErrorHandler.js";
import { DELETE_TRIP_SUCCESS } from "../../Utils/constants.js";

import deleteTripService from "../services/deleteTripService.js";

const deleteTripController = asyncErrorHandler(async (req, res, next) => {
  const trip_id = req.body.data;

  await deleteTripService(trip_id, req.user.role);

  return res
    .status(DELETE_TRIP_SUCCESS.status)
    .send({ message: DELETE_TRIP_SUCCESS.message });
});

export default deleteTripController;
