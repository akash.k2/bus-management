import asyncErrorHandler from "../../Utils/asyncErrorHandler.js";
import { FETCH_SUCCESS, NO_CONTENT } from "../../Utils/constants.js";

import viewTravelsTripsService from "../services/viewTravelsTripsService.js";

const viewTravelsTripsController = asyncErrorHandler(async (req, res, next) => {
  const { travels_id } = req.params;
  const { day } = req.query;

  const trips = await viewTravelsTripsService(
    req.user.id,
    travels_id,
    day,
    req.user.role
  );

  if (trips === "No trips") {
    return res.status(NO_CONTENT.status).send({ message: NO_CONTENT.message });
  } else if (trips) {
    return res.status(FETCH_SUCCESS.status).send({ trips });
  }
});

export default viewTravelsTripsController;
