const capitalizeWord = (word) => {
  const first = word.charAt(0).toUpperCase();
  const rest = word.slice(1).toLowerCase();

  return first + rest;
};

export default capitalizeWord;
