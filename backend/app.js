import dotenv from "dotenv";
dotenv.config();

import express, { json } from "express";
import bodyParser from "body-parser";
import { default as mongoose } from "mongoose";
import cors from "cors";
import CustomError from "./Utils/CustomError.js";
import authRouter from "./Auth/routes/routes.js";
import tripRouter from "./Trips/router/routes.js";
import bookingRouter from "./Bookings/routes/routes.js";
import globalErrorHandler from "./Utils/globalErrorHandler.js";
import { NOT_FOUND } from "./Utils/constants.js";

const app = express();

app.use(
  cors({
    origin: "*",
    credentials: true,
    optionsSuccessStatus: 200,
  })
);

app.use(json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(authRouter);
app.use(tripRouter);
app.use(bookingRouter);

app.all("*", (req, res, next) => {
  const error = new CustomError(NOT_FOUND.message, NOT_FOUND.status);
  next(error);
});

app.use(globalErrorHandler);

try {
  await mongoose.connect(process.env.MONGO_URL);
  console.log("DB connected");
  app.listen(process.env.PORT, () => {
    console.log("Server started");
  });
} catch {
  (err) => {
    console.log(err);
  };
}

export default app;
