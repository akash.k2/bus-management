import { Routes, Route, Navigate } from "react-router-dom";

import UserRoutes from "./Auth/routes/UserRoutes";
import UserHeader from "./Auth/layouts/UserHeader";

import Login from "./Auth/pages/Login";
import Register from "./Auth/pages/Register";
import UserHome from "./Auth/pages/UserHome";
import TripsDetail from "./Trips/pages/TripsDetail";
import Booking from "./Bookings/pages/Booking";
import MyBookings from "./Bookings/pages/MyBookings";
import Unauthorized from "./Common/pages/Unauthorized";

const App = () => {
  return (
    <Routes>
      <Route path="/" exact element={<Navigate to="/login" />} />
      <Route path="/login" element={<Login />} />
      <Route path="/register" element={<Register />} />
      <Route path="/unauthorized" element={<Unauthorized />} />
      <Route
        path="*"
        element={
          <UserRoutes>
            <UserHeader />
            <Routes>
              <Route path="/home" element={<UserHome />} />
              <Route path="/trips" element={<TripsDetail />} />
              <Route path="/booking" element={<Booking />} />
              <Route path="/my-bookings" element={<MyBookings />} />
              <Route path="/" element={<h1>No page</h1>} />
            </Routes>
          </UserRoutes>
        }
      />
    </Routes>
  );
};

export default App;
