import React from "react";
import styled from "styled-components";

import { Row, Col } from "reactstrap";
import TripsCard from "./TripsCard";

const TripsItem = (props) => {
  return (
    <section>
      <Row>
        {props.trips.map((trip) => {
          return (
            <Col lg="4" md="6" key={trip._id}>
              <Wrapper>
                <TripsCard trip={trip} date={props.date} />
              </Wrapper>
            </Col>
          );
        })}
      </Row>
    </section>
  );
};

export default TripsItem;

const Wrapper = styled.section`
  margin: 6% auto;
  width: 90%;
`;
