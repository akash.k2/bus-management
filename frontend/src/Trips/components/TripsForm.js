import { Fragment, useState } from "react";
import { FormGroup, Card, Row, Col, Button } from "reactstrap";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import styles from "./TripsForm.module.css";
import { places, toastConfig } from "../../Utils/constants";

import Select from "react-select";

import { toast, ToastContainer } from "react-toastify";
import { useDispatch } from "react-redux";
import { addSearchDetails } from "../../store/slices/searchSlice";

const TripsForm = (props) => {
  const dispatch = useDispatch();
  const [src, setSrc] = useState("");
  const [dest, setDest] = useState("");
  const [date, setDate] = useState(null);

  const options = places.map((place) => {
    return { label: place, value: place };
  });

  const submitHandler = async (event) => {
    event.preventDefault();
    try {
      if (src === "") {
        throw { msg: "Enter source" };
      }
      if (dest === "") {
        throw { msg: "Enter destination" };
      }
      if (date === null) {
        throw { msg: "Enter date" };
      }
      if (src === dest) {
        throw { msg: "Enter valid source and destination" };
      }
    } catch (err) {
      toast.error(err.msg, toastConfig);
      return;
    }
    const res = dispatch(addSearchDetails({ src, dest, date }));

    props.onSet(res.payload.src, res.payload.dest, res.payload.date);
  };

  return (
    <Fragment>
      <Row>
        <Col>
          <Card className={styles.card}>
            <div>
              <Row className={styles.row}>
                <Col lg="4">
                  <FormGroup className={styles.form_group}>
                    <Select
                      defaultValue={{
                        label: "Enter source",
                        value: "placeholder",
                      }}
                      options={options}
                      onChange={(src) => {
                        setSrc(src.value);
                      }}
                    />
                  </FormGroup>
                </Col>

                <Col lg="4">
                  <FormGroup className={styles.form_group}>
                    <Select
                      defaultValue={{
                        label: "Enter destination",
                        value: "placeholder",
                      }}
                      options={options}
                      onChange={(dest) => {
                        setDest(dest.value);
                      }}
                    />
                  </FormGroup>
                </Col>

                <Col lg="4">
                  <div className={styles.date}>
                    <FormGroup>
                      <DatePicker
                        showIcon
                        placeholderText="Enter date"
                        dateFormat="dd-MM-yyyy"
                        selected={date}
                        minDate={new Date()}
                        onChange={(date) => setDate(date)}
                        style={{ maxWidth: "90%" }}
                      />
                    </FormGroup>
                  </div>
                </Col>
              </Row>
              <center className={styles.btn}>
                <Button color="danger" onClick={submitHandler}>
                  Search Bus
                </Button>
              </center>
            </div>
          </Card>
        </Col>
      </Row>
      <ToastContainer />
    </Fragment>
  );
};

export default TripsForm;
