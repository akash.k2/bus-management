import React, { Fragment } from "react";

import { useNavigate } from "react-router-dom";
import styles from "./TripsCard.module.css";

import {
  Card,
  CardBody,
  CardTitle,
  CardHeader,
  CardText,
  Row,
  Col,
  Button,
} from "reactstrap";

import { FaRupeeSign } from "react-icons/fa";

const TripsCard = (props) => {
  const navigate = useNavigate();

  let end_time =
    (+props.trip.start_time.toString().split(":")[0] + props.trip.duration) %
    24;

  if (end_time <= 9) {
    end_time = "0" + end_time;
  }

  const bookedSeats = props.trip.seat_cap - props.trip.available_seats;

  return (
    <Fragment>
      <Card body className={styles.card}>
        <CardHeader className={styles.card_header}>
          <center>{props.trip.travels_name}</center>
        </CardHeader>
        <CardBody>
          <Row>
            <Col>
              <CardTitle tag="h5">{props.trip.src}</CardTitle>
            </Col>
            <Col>
              <CardTitle tag="h5">{props.trip.dest}</CardTitle>
            </Col>
          </Row>

          <CardText>
            <Row>
              <Col>
                <CardTitle>{props.trip.start_time}</CardTitle>
              </Col>
              <Col>
                <CardTitle>{end_time}:00</CardTitle>
              </Col>
            </Row>
          </CardText>

          <CardText>
            <Row>
              <Col>
                <CardTitle>
                  <FaRupeeSign size={"7%"} /> {"  "}Rs.{props.trip.price}
                </CardTitle>
              </Col>
              <Col>
                <CardTitle>
                  <img src="images/seats.png" alt="" width={"18%"} />{" "}
                  {bookedSeats} / {props.trip.seat_cap}
                </CardTitle>
              </Col>
            </Row>
          </CardText>
          <center>
            <Button
              color="danger"
              onClick={() => {
                navigate(
                  `/booking?trip_id=${props.trip._id}&date=${props.date}`
                );
              }}
            >
              Book Tickets
            </Button>
          </center>
        </CardBody>
      </Card>
    </Fragment>
  );
};

export default TripsCard;
