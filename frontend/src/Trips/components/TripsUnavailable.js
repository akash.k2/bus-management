import React from "react";
import styles from "./TripsUnavailable.module.css";

const TripsUnavailable = () => {
  return (
    <center className={styles.main}>
      <img src="images/oops.png" alt="" width={"15%"} />
      <h5 className={styles.h5}>
        Sorry, no buses are available on this route for the selected date
      </h5>
    </center>
  );
};

export default TripsUnavailable;
