import React, { Fragment } from "react";
import TripsForm from "../components/TripsForm";

import { toast, ToastContainer } from "react-toastify";

// import { useNavigate } from "react-router";
import { fetchTrips, getTripState } from "../../store/slices/tripSlice";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";

import {
  FORBIDDEN,
  LOADING,
  SUCCEEDED,
  toastConfig,
} from "../../Utils/constants";

import TripsItem from "../components/TripsItem";
import TripsUnavailable from "../components/TripsUnavailable";
import { forbiddenCheck } from "../../Utils/forbiddenCheck";
import { getSearchDetails } from "../../store/slices/searchSlice";

const TripsDetail = () => {
  // const [date, setDate] = useState("");

  const {
    error: tripError,
    status: tripStatus,
    trips: fetchedTrips,
  } = useSelector(getTripState);

  const { date, isEmpty } = useSelector(getSearchDetails);

  const dispatch = useDispatch();
  const navigate = useNavigate();

  const onSetHandler = async (src, dest, date) => {
    dispatch(fetchTrips({ src, dest, date }));
  };

  let content = null;

  if (tripStatus === LOADING) {
    content = (
      <center>
        <img src="images/bus-gif.gif" alt="" width={"15%"} />
      </center>
    );
  } else if (tripStatus === SUCCEEDED && fetchedTrips.length < 1) {
    content = <TripsUnavailable />;
  } else if (!isEmpty && tripStatus === SUCCEEDED && fetchedTrips.length > 1) {
    content = (
      <TripsItem trips={fetchedTrips} date={new Date(date).toDateString()} />
    );
  } else if (tripError === FORBIDDEN.message) {
    forbiddenCheck(FORBIDDEN.message, dispatch, navigate);
  } else if (tripError) {
    toast.error(tripError, toastConfig);
  }

  return (
    <Fragment>
      <TripsForm onSet={onSetHandler} />
      {content}
      <ToastContainer />
    </Fragment>
  );
};

export default TripsDetail;
