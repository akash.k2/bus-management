import React, { useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { Button } from "reactstrap";
import styles from "./Unauthorized.module.css";
import { removeInfo } from "../../store/slices/userSlice";
import { useDispatch } from "react-redux";
import { removeTripInfo } from "../../store/slices/tripSlice";
import { removeSearchDetails } from "../../store/slices/searchSlice";

const Unauthorized = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(removeTripInfo());
    dispatch(removeSearchDetails());
    sessionStorage.clear();
  }, []);

  return (
    <div className={styles.main}>
      <center>
        <img src="images/error.png" alt="" width={"30%"} />
        <h3 className={styles.h3}>Unauthorized...</h3>
        <Button
          color="danger"
          onClick={() => {
            dispatch(removeInfo());
            navigate("/login");
          }}
        >
          Login to proceed
        </Button>
      </center>
    </div>
  );
};

export default Unauthorized;
