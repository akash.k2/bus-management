import { toast } from "react-toastify";

export const USER = "user";

export const TRAVELS = "travels";

export const PASSWORD_MESSAGE =
  "Password must be minimum 8 characters, with at least a symbol, upper and lower case letters and a number";

export const PHONE_MESSAGE = "Phone number must be 10 digits";

export const PASSWORD_REGEX =
  /^(?=.*\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z]).{8,}$/;

export const CONFIRM_PASSWORD = "Confirmed password must be same as password";

export const LOADING = "loading";

export const SUCCEEDED = "succeeded";

export const IDLE = "idle";

export const FAILED = "failed";

export const REGISTER_SUCCESS = {
  status: 201,
  message: "Registered successfully",
};

export const FETCH_SUCCESS = {
  status: 200,
};

export const LOGIN_SUCCESS = {
  status: 200,
  message: "Logged in successfully",
};

export const CANCEL_BOOKING_SUCCESS = {
  status: 200,
  message: "Booking cancelled successfully",
};

export const BOOKING_SUCCESS = {
  status: 201,
  message: "Booking done successfully",
};

export const NO_CONTENT = {
  status: 204,
  message: "No content available",
};

export const STANDARD_ERROR = {
  status: 400,
};

export const UNAUTHORIZED = {
  status: 401,
  message: "Invalid credentials",
};

export const FORBIDDEN = {
  status: 403,
  message: "Unauthorized",
};

export const ERROR_UNKNOWN = {
  status: 500,
  message: "Something went wrong!",
};

export const toastConfig = {
  position: toast.POSITION.BOTTOM_RIGHT,
  autoClose: 1500,
  theme: "dark",
};

export const places = [
  "Ariyalur",
  "Chengalpattu",
  "Chennai",
  "Coimbatore",
  "Cuddalore",
  "Dharmapuri",
  "Dindigul",
  "Erode",
  "Kallakurichi",
  "Kanchipuram",
  "Kanniyakumari",
  "Karur",
  "Krishnagiri",
  "Madurai",
  "Mayiladuthurai",
  "Nagapattinam",

  "Namakkal",

  "Nilgiris",

  "Perambalur",

  "Pudukkottai",

  "Ramanathapuram",

  "Ranipet",

  "Salem",

  "Sivagangai",

  "Tenkasi",

  "Thanjavur",

  "Theni",

  "Thoothukudi",

  "Tiruchirappalli",

  "Tirunelveli",

  "Tirupathur",

  "Tiruppur",

  "Tiruvallur",

  "Tiruvannamalai",

  "Tiruvarur",

  "Vellore",

  "Viluppuram",

  "Virudhunagar",
];
