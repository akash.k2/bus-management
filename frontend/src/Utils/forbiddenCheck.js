import { removeSearchDetails } from "../store/slices/searchSlice";
import { removeTripInfo } from "../store/slices/tripSlice";
import { removeInfo } from "../store/slices/userSlice";
import { FORBIDDEN } from "./constants";

export const forbiddenCheck = (err, dispatch, navigate) => {
  let errorMsg;
  let errorStatus;
  errorMsg = err?.response?.data?.message || FORBIDDEN.message;
  errorStatus = err?.response?.status || 403;
  if (errorStatus === FORBIDDEN.status && errorMsg === FORBIDDEN.message) {
    dispatch(removeTripInfo());
    dispatch(removeInfo());
    dispatch(removeSearchDetails());
    sessionStorage.clear();
    navigate("/unauthorized");
    return;
  }
};
