import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";

import axios from "axios";
import {
  FAILED,
  IDLE,
  LOADING,
  NO_CONTENT,
  SUCCEEDED,
} from "../../Utils/constants";

const initialState = {
  trips: [],
  status: IDLE,
  error: null,
};

export const fetchTrips = createAsyncThunk(
  "trips/fetchTrips",
  async ({ src, dest, date }) => {
    try {
      const res = await axios.get(
        `/view-trips?src=${src}&dest=${dest}&date=${date}`
      );
      if (res.status === NO_CONTENT.status) {
        return [];
      } else {
        return res.data.trips;
      }
    } catch (err) {
      const error = err;
      error.message = err.response.data?.message;
      throw error;
    }
  }
);

const tripSlice = createSlice({
  name: "trip",
  initialState,
  reducers: {
    removeTripInfo(state) {
      state.trips = [];
      state.status = IDLE;
      state.error = null;
    },
  },
  extraReducers(builder) {
    builder
      .addCase(fetchTrips.pending, (state) => {
        state.trips = [];
        state.status = LOADING;
        state.error = null;
      })
      .addCase(fetchTrips.fulfilled, (state, action) => {
        state.trips = action.payload;
        state.status = SUCCEEDED;
        state.error = null;
      })
      .addCase(fetchTrips.rejected, (state, action) => {
        state.trips = [];
        state.status = FAILED;
        state.error = action.error.message;
      });
  },
});

export const { removeTripInfo } = tripSlice.actions;

export const getTripState = (state) => state.trip;

export const tripReducer = tripSlice.reducer;
