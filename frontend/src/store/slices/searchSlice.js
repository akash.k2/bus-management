import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  src: "",
  dest: "",
  date: "",
  isEmpty: true,
};

export const searchSlice = createSlice({
  name: "searchDetails",
  initialState,
  reducers: {
    addSearchDetails(state, action) {
      state.src = action.payload.src;
      state.dest = action.payload.dest;
      state.date = action.payload.date;
      state.isEmpty = false;
    },
    removeSearchDetails(state) {
      state.src = "";
      state.dest = "";
      state.date = "";
      state.isEmpty = true;
    },
  },
});

export const { addSearchDetails, removeSearchDetails } = searchSlice.actions;

export const getSearchDetails = (state) => state.searchDetails;

export const searchDetailsReducer = searchSlice.reducer;
