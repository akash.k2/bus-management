import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
import {
  FAILED,
  FORBIDDEN,
  IDLE,
  LOADING,
  SUCCEEDED,
  LOGIN_SUCCESS,
  REGISTER_SUCCESS,
} from "../../Utils/constants";

const initialState = {
  name: null,
  id: null,
  role: null,
  status: IDLE,
  error: null,
};

export const fetchUserDetails = createAsyncThunk(
  "user/fetchUserDetails",
  async () => {
    try {
      const res = await axios.get("/get-details");
      return res.data.details;
    } catch (err) {
      const error = err;
      error.message = FORBIDDEN.message;
      throw error;
    }
  }
);

export const userLogin = createAsyncThunk("user/userLogin", async (details) => {
  try {
    const res = await axios.post("/login", details);
    if (
      res?.status === LOGIN_SUCCESS.status &&
      res?.data?.message === LOGIN_SUCCESS.message
    ) {
      sessionStorage.setItem("accessToken", res.data.token);
      return res.data.details;
    }
  } catch (err) {
    const error = err;
    error.message = err.response.data?.message;
    throw error;
  }
});

export const userRegister = createAsyncThunk(
  "user/userRegister",
  async (details) => {
    try {
      const res = await axios.post("/register", details);
      if (
        res?.status === REGISTER_SUCCESS.status &&
        res?.data?.message === REGISTER_SUCCESS.message
      ) {
        sessionStorage.setItem("accessToken", res.data.token);
        return res.data.details;
      }
    } catch (err) {
      const error = err;
      error.message = err.response.data?.message;
      throw error;
    }
  }
);

const userSlice = createSlice({
  name: "user",
  initialState: initialState,
  reducers: {
    addInfo(state, action) {
      state.name = action.payload.name;
      state.id = action.payload.id;
      state.role = action.payload.role;
      state.status = SUCCEEDED;
      state.error = null;
    },
    removeInfo(state) {
      state.name = null;
      state.id = null;
      state.role = null;
      state.status = IDLE;
      state.error = null;
    },
  },
  extraReducers(builder) {
    builder
      .addCase(userLogin.fulfilled, (state, action) => {
        state.name = action.payload.name;
        state.id = action.payload.id;
        state.role = action.payload.role;
        state.error = null;
        state.status = SUCCEEDED;
      })
      .addCase(userLogin.rejected, (state, action) => {
        state.error = action.error.message;
        state.id = null;
        state.name = null;
        state.role = null;
        state.status = FAILED;
      })
      .addCase(userRegister.fulfilled, (state, action) => {
        state.name = action.payload.name;
        state.id = action.payload.id;
        state.role = action.payload.role;
        state.error = null;
        state.status = SUCCEEDED;
      })
      .addCase(userRegister.rejected, (state, action) => {
        state.error = action.error.message;
        state.id = null;
        state.name = null;
        state.role = null;
        state.status = FAILED;
      })
      .addCase(fetchUserDetails.pending, (state) => {
        state.status = LOADING;
        state.error = null;
        state.id = null;
        state.name = null;
        state.role = null;
      })
      .addCase(fetchUserDetails.fulfilled, (state, action) => {
        state.status = SUCCEEDED;
        state.error = null;
        state.name = action.payload.name;
        state.id = action.payload.id;
        state.role = action.payload.role;
      })
      .addCase(fetchUserDetails.rejected, (state, action) => {
        state.status = FAILED;
        state.error = action.error.message;
        state.id = null;
        state.name = null;
        state.role = null;
      });
  },
});

export const { addInfo, removeInfo } = userSlice.actions;

export const getUserState = (state) => state.user;

export const userReducer = userSlice.reducer;
