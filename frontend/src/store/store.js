import { configureStore } from "@reduxjs/toolkit";
import { userReducer } from "./slices/userSlice.js";
import { tripReducer } from "./slices/tripSlice.js";
import { searchDetailsReducer } from "./slices/searchSlice.js";

const store = configureStore({
  reducer: {
    user: userReducer,
    trip: tripReducer,
    searchDetails: searchDetailsReducer,
  },
});

export default store;
