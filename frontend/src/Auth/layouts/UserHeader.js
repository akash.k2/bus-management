import React, { useState } from "react";
import { MdLogout } from "react-icons/md";
import styled from "styled-components";
import { useNavigate } from "react-router";
import { removeInfo } from "../../store/slices/userSlice";
import { useDispatch } from "react-redux";

import "./UserHeader.css";

import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  Button,
} from "reactstrap";
import { removeTripInfo } from "../../store/slices/tripSlice";
import { removeSearchDetails } from "../../store/slices/searchSlice";

const UserHeader = () => {
  const [isOpen, setIsOpen] = useState(false);
  const navigate = useNavigate();
  const toggle = () => setIsOpen((isOpen) => !isOpen);
  const dispatch = useDispatch();

  return (
    <header style={{ backgroundColor: "#241F22", color: "#F2F0F1" }}>
      <Navbar expand="md">
        <NavbarBrand style={{ color: "#F2F0F1", fontWeight: "inherit" }}>
          <img
            alt="logo"
            src="images/bus_icon.png"
            style={{
              height: "40%",
              width: "40%",
              marginRight: "",
            }}
          />
          <Button
            style={{ background: "#241F22", border: "none" }}
            size="lg"
            onClick={() => {
              navigate("/home");
            }}
          >
            TicketZa
          </Button>
        </NavbarBrand>

        <NavbarToggler onClick={toggle} />
        <Collapse isOpen={isOpen} navbar style={{ flexGrow: "0" }}>
          <Nav className="me-auto" navbar>
            <NavItems>
              <NavItem style={{ cursor: "pointer" }}>
                <Button
                  style={{ background: "#241F22", border: "none" }}
                  size="lg"
                  onClick={() => {
                    navigate("/trips");
                  }}
                >
                  Book Tickets
                </Button>
              </NavItem>
              <NavItem style={{ cursor: "pointer" }}>
                <Button
                  style={{ background: "#241F22", border: "none" }}
                  size="lg"
                  onClick={() => {
                    navigate("/my-bookings");
                  }}
                >
                  My Bookings
                </Button>
              </NavItem>

              <NavItem style={{ cursor: "pointer" }}>
                <MdLogout
                  onClick={() => {
                    dispatch(removeInfo());
                    dispatch(removeTripInfo());
                    dispatch(removeSearchDetails());
                    sessionStorage.clear();
                    navigate("/login");
                  }}
                />
              </NavItem>
            </NavItems>
          </Nav>
        </Collapse>
      </Navbar>
    </header>
  );
};

export default UserHeader;

const NavItems = styled.section`
  align-items: center;
  display: flex;
  justify-content: center;
`;
