import React from "react";
import { Row, Col } from "reactstrap";
import styles from "./UserFooter.module.css";
import {
  BsFacebook,
  BsTwitter,
  BsInstagram,
  BsFillEnvelopeAtFill,
} from "react-icons/bs";

const UserFooter = () => {
  return (
    <footer className={styles.footer}>
      <Row className={styles.row}>
        <Col lg="4">
          <a href="http://www.facebook.com/" role="button">
            <BsFacebook className={styles.social_icon} color="white" />
          </a>
          <a href="https://twitter.com/explore" role="button">
            <BsTwitter className={styles.social_icon} color="white" />
          </a>
          <a href="https://www.instagram.com/" role="button">
            <BsInstagram className={styles.social_icon} color="white" />
          </a>
          <a href="mailto:akashdevelops@gmail.com" role="button">
            <BsFillEnvelopeAtFill
              className={styles.social_icon}
              color="white"
            />
          </a>
        </Col>
        <Col lg="4">
          <p>©copyrights by TicketZa</p>
        </Col>
        <Col lg="4">Contact us: &nbsp; admin@ticketZa, &nbsp;9876543210</Col>
      </Row>
    </footer>
  );
};

export default UserFooter;
