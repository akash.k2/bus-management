import { useEffect } from "react";

import { useNavigate } from "react-router";
import { useDispatch } from "react-redux";
import { fetchUserDetails, getUserState } from "../../store/slices/userSlice";
import { useSelector } from "react-redux";

import { removeInfo } from "../../store/slices/userSlice";

import {
  FAILED,
  FORBIDDEN,
  IDLE,
  LOADING,
  SUCCEEDED,
  USER,
} from "../../Utils/constants";
import { removeTripInfo } from "../../store/slices/tripSlice";
import { withErrorBoundary } from "react-error-boundary";
import Unauthorized from "../../Common/pages/Unauthorized";
import { removeSearchDetails } from "../../store/slices/searchSlice";

const UserRoutes = (props) => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const authToken = sessionStorage.getItem("accessToken");
  if (!authToken) {
    dispatch(removeInfo());
    dispatch(removeSearchDetails());
    dispatch(removeTripInfo());
    navigate("/unauthorized");
  }

  const { role: userRole, error, status } = useSelector(getUserState);

  useEffect(() => {
    if (status === IDLE) {
      dispatch(fetchUserDetails());
    }
  }, []);

  if (status === FAILED && (error === FORBIDDEN.message || userRole !== USER)) {
    dispatch(removeTripInfo());
    dispatch(removeInfo());
    sessionStorage.clear();
    navigate("/unauthorized");
    throw new Error("Unauthorized");
  }

  if (status === LOADING) return null;

  if (status === SUCCEEDED) return props.children;
};

const UserRoutesWithErrorBoundary = withErrorBoundary(UserRoutes, {
  fallbackRender: (props) => {
    return <Unauthorized />;
  },
});
export default UserRoutesWithErrorBoundary;
