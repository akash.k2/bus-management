// import { useEffect } from "react";

// import { useNavigate } from "react-router";
// import { useDispatch } from "react-redux";
// import {
//   fetchUserDetails,
//   getUserDetailsError,
//   getUserDetailsStatus,
//   getUserRole,
// } from "../../store/slices/userSlice";
// import { useSelector } from "react-redux";

// import { removeInfo } from "../../store/slices/userSlice";

// import { FAILED, FORBIDDEN, IDLE, LOADING } from "../../Utils/constants";

// const UserRoutes = (props) => {
//   const dispatch = useDispatch();
//   const navigate = useNavigate();

//   const userRole = useSelector(getUserRole);
//   const status = useSelector(getUserDetailsStatus);
//   const error = useSelector(getUserDetailsError);

//   useEffect(() => {
//     dispatch(fetchUserDetails());
//   }, []);

//   if (status === FAILED && error === FORBIDDEN.message) {
//     dispatch(removeInfo());
//     sessionStorage.clear();
//     navigate("/unauthorized");
//     return;
//   }

//   if (status === IDLE || status === LOADING) return null;

//   return props.children;
// };
// export default UserRoutes;
