import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  MDBContainer,
  MDBRow,
  MDBCol,
  MDBCard,
  MDBCardBody,
  MDBInput,
} from "mdb-react-ui-kit";
import { useNavigate } from "react-router";

import styles from "./Login.module.css";

import { Link } from "react-router-dom";

import { Button, ButtonGroup } from "reactstrap";
import { getUserState, userLogin } from "../../store/slices/userSlice";
import { SUCCEEDED } from "../../Utils/constants";

const Login = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [role, setRole] = useState("user");

  const navigate = useNavigate();
  const dispatch = useDispatch();

  const {
    name: userName,
    id: userId,
    role: userRole,
    error: errMsg,
    status,
  } = useSelector(getUserState);

  const emailHandler = (event) => {
    setEmail(event.target.value);
  };
  const passwordHandler = (event) => {
    setPassword(event.target.value);
  };

  const submitHandler = async (event) => {
    event.preventDefault();

    if (email.trim() === "") return;

    const details = {
      email: email.trim(),
      password: password,
      role: role,
    };

    dispatch(userLogin(details));
  };

  if (status === SUCCEEDED && userName && userId && userRole) {
    navigate("/home");
    return;
  }

  return (
    <MDBContainer fluid>
      <MDBRow className="d-flex justify-content-center align-items-center">
        <MDBCol col="12">
          <MDBCard className={styles.login_card}>
            <MDBCardBody className="p-5 w-100 d-flex flex-column">
              <h2 className={styles.login_h2 + " fw-bold text-center"}>
                Sign in
              </h2>
              <p className="fw-bold text-black-50 mb-3">Login as :</p>
              <ButtonGroup className={styles.button_group}>
                <Button
                  color="danger"
                  outline
                  onClick={() => setRole("user")}
                  active={role === "user"}
                >
                  User
                </Button>
                <Button
                  color="danger"
                  outline
                  onClick={() => setRole("travels")}
                  active={role === "travels"}
                >
                  Travel Agency
                </Button>
              </ButtonGroup>
              <form onSubmit={submitHandler}>
                <MDBInput
                  wrapperClass="mb-4 w-100"
                  placeholder="Email address"
                  id="email"
                  type="email"
                  size="lg"
                  required
                  onChange={emailHandler}
                  value={email}
                />
                <MDBInput
                  wrapperClass="mb-4 w-100"
                  placeholder="Password"
                  id="password"
                  type="password"
                  size="lg"
                  required
                  onChange={passwordHandler}
                  value={password}
                />
                <center>
                  <p className={styles.err_msg}>{errMsg}</p>
                </center>
                <center>
                  <Button size="lg" color="danger" type="submit">
                    Login
                  </Button>
                </center>
              </form>
              <hr className="my-4" />
              <div className="d-flex flex-row align-items-center justify-content-center mb-1">
                <p className="mb-0">
                  New User ?{" "}
                  <Link to="/register" style={{ textDecoration: "none" }}>
                    Sign up
                  </Link>
                </p>
              </div>
            </MDBCardBody>
          </MDBCard>
        </MDBCol>
      </MDBRow>
    </MDBContainer>
  );
};

export default Login;
