import React, { useRef, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  MDBContainer,
  MDBRow,
  MDBCol,
  MDBCard,
  MDBCardBody,
  MDBInput,
} from "mdb-react-ui-kit";
import { useNavigate } from "react-router";

import { Link } from "react-router-dom";

import styles from "./Register.module.css";

import { Button, ButtonGroup } from "reactstrap";

import { getUserState, userRegister } from "../../store/slices/userSlice";

import {
  CONFIRM_PASSWORD,
  PASSWORD_REGEX,
  PASSWORD_MESSAGE,
  PHONE_MESSAGE,
  SUCCEEDED,
} from "../../Utils/constants";

function Login() {
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const [role, setRole] = useState("user");
  const [error, setError] = useState("");

  const {
    name: userName,
    id: userId,
    role: userRole,
    error: errMsg,
    status,
  } = useSelector(getUserState);

  const nameRef = useRef();
  const emailRef = useRef();
  const phoneRef = useRef();
  const passRef = useRef();
  const cnf_passRef = useRef();

  const submitHandler = async (event) => {
    event.preventDefault();
    const name = nameRef.current.value.trim();
    const email = emailRef.current.value.trim();
    const phone = phoneRef.current.value.trim();
    const password = passRef.current.value;
    const cnf_pass = cnf_passRef.current.value;
    if (name === "") return;
    if (email === "") return;
    if (phone.length !== 10) {
      setError(PHONE_MESSAGE);
      return;
    }

    var regex = PASSWORD_REGEX;

    if (!regex.test(password)) {
      setError(PASSWORD_MESSAGE);
      return;
    }

    if (cnf_pass !== password) {
      setError(CONFIRM_PASSWORD);
      return;
    }

    setError("");

    const details = {
      name: name,
      phone: phone,
      email: email,
      password: password,
      role: role,
    };

    dispatch(userRegister(details));
  };

  if (status === SUCCEEDED && userName && userId && userRole) {
    navigate("/home");
    return;
  }

  return (
    <MDBContainer fluid>
      <MDBRow className="d-flex justify-content-center align-items-center">
        <MDBCol col="12">
          <MDBCard className={styles.register_card}>
            <MDBCardBody
              className={
                styles.register_card_body + " w-100 d-flex flex-column"
              }
            >
              <h3 className={styles.register_h3 + " fw-bold text-center"}>
                Register
              </h3>
              <p className="fw-bold text-black-50 mb-2">Register as :</p>
              <ButtonGroup className={styles.register_button_grp}>
                <Button
                  color="danger"
                  outline
                  onClick={() => setRole("user")}
                  active={role === "user"}
                >
                  User
                </Button>
                <Button
                  color="danger"
                  outline
                  onClick={() => setRole("travels")}
                  active={role === "travels"}
                >
                  Travel Agency
                </Button>
              </ButtonGroup>
              <form onSubmit={submitHandler}>
                <MDBInput
                  wrapperClass="mb-4 w-100"
                  placeholder="Name"
                  id="name"
                  type="text"
                  size="md"
                  required
                  ref={nameRef}
                />
                <MDBInput
                  wrapperClass="mb-4 w-100"
                  placeholder="Email address"
                  id="email"
                  type="email"
                  size="md"
                  required
                  ref={emailRef}
                />
                <MDBInput
                  wrapperClass="mb-4 w-100"
                  placeholder="Phone Number"
                  id="phone"
                  type="number"
                  size="md"
                  required
                  ref={phoneRef}
                />
                <MDBInput
                  wrapperClass="mb-4 w-100"
                  placeholder="Password"
                  id="password"
                  type="password"
                  size="md"
                  required
                  ref={passRef}
                />
                <MDBInput
                  wrapperClass="mb-4 w-100"
                  placeholder="Confirm Password"
                  id="cnf_password"
                  type="password"
                  size="md"
                  required
                  ref={cnf_passRef}
                />

                <center>
                  <p className={styles.register_errMsg}>
                    {error ? error : errMsg}
                  </p>
                </center>

                <center>
                  <Button size="lg" color="danger" type="submit">
                    Register
                  </Button>
                </center>
              </form>
              <hr className="my-3" />
              <div className="d-flex flex-row align-items-center justify-content-center">
                <p>
                  Already an user ?{" "}
                  <Link to="/login" style={{ textDecoration: "none" }}>
                    Sign in
                  </Link>
                </p>
              </div>
            </MDBCardBody>
          </MDBCard>
        </MDBCol>
      </MDBRow>
    </MDBContainer>
  );
}

export default Login;
