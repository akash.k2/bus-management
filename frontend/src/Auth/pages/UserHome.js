import { Row, Col, Button } from "reactstrap";
import { AiFillCheckCircle, AiFillHeart } from "react-icons/ai";
import { BiBullseye } from "react-icons/bi";
import styles from "./UserHome.module.css";
import { Fragment } from "react";
import { useNavigate } from "react-router-dom";

const UserHome = () => {
  const navigate = useNavigate();
  return (
    <Fragment>
      <section className={styles.first_section}>
        <div className={styles.container_fluid}>
          <Row>
            <Col lg="6">
              <center>
                <h1 className={styles.title}>More than just booking tickets</h1>
                <Row>
                  <Col>
                    <Button
                      className={styles.btn}
                      color="light"
                      size="lg"
                      onClick={() => {
                        navigate("/trips");
                      }}
                    >
                      Book tickets
                    </Button>
                  </Col>
                </Row>
              </center>
            </Col>
            <Col lg="6">
              <center>
                <img
                  className={styles.bus_img}
                  src="images/bus_home.png"
                  alt="bus-img"
                />
              </center>
            </Col>
          </Row>
        </div>
      </section>

      <section className={styles.features}>
        <Row className={styles.row}>
          <Col className={styles.feature_box} lg="4">
            <center>
              <AiFillCheckCircle className={styles.ft_icon} size={"80px"} />
              <h3>Fast booking.</h3>
              <p>Book and confirm tickets in seconds.</p>
            </center>
          </Col>
          <Col className={styles.feature_box} lg="4">
            <center>
              <BiBullseye className={styles.ft_icon} size={"80px"} />
              <h3>High class facilty</h3>
              <p>Enjoy top class luxury facilities.</p>
            </center>
          </Col>
          <Col className={styles.feature_box} lg="4">
            <center>
              <AiFillHeart className={styles.ft_icon} size={"80px"} />
              <h3>Affordable rate</h3>
              <p>Get best rooms at affordable rate.</p>
            </center>
          </Col>
        </Row>
      </section>

      <section className={styles.about_us}>
        <h3>About us</h3>
        <p className={styles.about_us_para}>
          TicketZa is India's largest online bus ticketing platform that has
          transformed bus travel in the country by bringing ease and convenience
          to millions of Indians who travel using buses. By providing widest
          choice, superior customer service, lowest prices and unmatched
          benefits, TicketZa has served over 18 million customers.
        </p>
      </section>
    </Fragment>
  );
};

export default UserHome;
