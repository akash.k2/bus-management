import React, { Fragment, useEffect, useState } from "react";
import { Card, Row, Col, FormGroup, Label, Input, Button } from "reactstrap";
import "./SeatPicker.css";
import styles from "./SeatPicker.module.css";
import { toast, ToastContainer } from "react-toastify";

import Swal from "sweetalert2";
import { toastConfig } from "../../Utils/constants";
import { useNavigate } from "react-router-dom";

const SeatPicker = (props) => {
  const initialSeats = [];
  const navigate = useNavigate();
  const price = props.price;
  const seat_cap = props.seat_cap;
  const bookedSeats = props.bookedSeats;

  const [seats, setSeats] = useState([]);

  const [selectedSeats, setSelectedSeats] = useState([]);

  const [names, setNames] = useState([]);

  const [ages, setAges] = useState([]);

  useEffect(() => {
    for (let i = 1; i <= seat_cap; i++) {
      if (bookedSeats.includes(i)) {
        initialSeats.push({ id: i, label: `Seat ${i}`, disabled: true });
      } else {
        initialSeats.push({ id: i, label: `Seat ${i}`, disabled: false });
      }
    }

    setSeats(initialSeats);
  }, []);

  const nameChangeHandler = (idx, value) => {
    const updatedNames = [...names];
    updatedNames[idx] = value.trim();
    setNames(updatedNames);
  };

  const ageChangeHandler = (idx, value) => {
    const updatedAges = [...ages];
    updatedAges[idx] = value.trim();
    setAges(updatedAges);
  };

  const handleSeatClick = (seatId) => {
    const selectedSeat = seats.find((seat) => seat.id === seatId);

    if (selectedSeat.disabled) {
      return;
    }

    if (selectedSeats.includes(seatId)) {
      setSelectedSeats((prev) => prev.filter((id) => id !== seatId));
      names.pop();
      ages.pop();
    } else {
      setSelectedSeats((prev) => [...prev, seatId]);
    }
  };

  const submitHandler = async () => {
    const details = [];
    try {
      if (
        names.length === ages.length &&
        names.length === selectedSeats.length
      ) {
        names.forEach((passName, idx) => {
          if (passName.length !== 0 && ages[idx].length !== 0) {
            if (ages[idx] > 0 && ages[idx] <= 100)
              details.push({ name: passName, age: ages[idx] });
            else throw { msg: "Age should be between 1 and 100" };
          } else {
            throw { msg: "Enter valid passenger details" };
          }
        });

        const userRes = await Swal.fire({
          title: "Do you want to proceed?",
          showCancelButton: true,
          confirmButtonColor: "#3085d6",
          cancelButtonColor: "#d33",
          confirmButtonText: "Yes",
        });
        if (userRes.isConfirmed) {
          props.onSubmit({
            passDetails: details,
            totalPrice: totalPrice,
            seatNumbers: selectedSeats,
          });
          return;
        }
      } else {
        throw { msg: "Enter valid passenger details" };
      }
    } catch (err) {
      toast.error(err.msg, toastConfig);
    }
  };

  let totalPrice = price * selectedSeats.length;

  return (
    <Row>
      <Col>
        <h2 className={styles.h2_heading}>Pick your seats</h2>
        <Card className={styles.seat_card}>
          <div className="seat-container">
            {seats.map((seat) => (
              <button
                style={{ width: "40%" }}
                key={seat.id}
                onClick={() => handleSeatClick(seat.id)}
                disabled={seat.disabled}
                className={`seat ${
                  selectedSeats.includes(seat.id) ? "selected" : ""
                } ${seat.disabled ? "disabled" : ""}`}
              >
                {seat.label}
              </button>
            ))}
          </div>
        </Card>
      </Col>
      <Col>
        {bookedSeats?.length === seat_cap && (
          <center className={styles.unavailable}>
            <img src="images/oops.png" alt="" width={"60%"} />
            <h3 className={styles.seatpicker_h3}>
              Sorry, no seats are available
            </h3>
            <Button
              color="light"
              onClick={() => {
                navigate("/trips");
              }}
            >
              Choose another trip
            </Button>
          </center>
        )}
        {selectedSeats.length > 0 && (
          <Card className={styles.details_card}>
            {selectedSeats.map((seat, idx) => (
              <Fragment key={seat}>
                <FormGroup>
                  <Label for={seat}>{`Passenger ${idx + 1}`}</Label>
                  <Row>
                    <Col>
                      <Input
                        id={seat}
                        name="name"
                        placeholder="Name"
                        type="text"
                        required
                        onChange={(e) => nameChangeHandler(idx, e.target.value)}
                      />
                    </Col>
                    <Col>
                      <Input
                        id={seat}
                        name="age"
                        placeholder="Age"
                        type="number"
                        min={1}
                        max={100}
                        required
                        onChange={(e) => ageChangeHandler(idx, e.target.value)}
                      />
                    </Col>
                  </Row>
                </FormGroup>
              </Fragment>
            ))}
            <Row>
              <Col>
                <center>
                  <h5>Total Price : {totalPrice}</h5>
                </center>
              </Col>

              <Col>
                <center>
                  <Button color="danger" onClick={submitHandler}>
                    Continue
                  </Button>
                </center>
              </Col>
            </Row>
          </Card>
        )}
      </Col>
      <ToastContainer />
    </Row>
  );
};

export default SeatPicker;
