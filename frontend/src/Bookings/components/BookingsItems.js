import React, { Fragment } from "react";
import { Row, Col } from "reactstrap";
import styled from "styled-components";
import BookingsCard from "./BookingsCard";
import styles from "./BookingsItems.module.css";

const BookingsItems = (props) => {
  return (
    <Fragment>
      <h2 className={styles.booking_headings}>Upcoming Bookings</h2>
      {props.currBookings.length > 0 ? (
        <Row>
          {props.currBookings.map((currBooking) => {
            return (
              <Col lg="4" md="6" key={currBooking._id}>
                <Wrapper>
                  <BookingsCard booking={currBooking} isCancel={true} />
                </Wrapper>
              </Col>
            );
          })}
        </Row>
      ) : (
        <center>
          <img src="images/no_bookings.png" alt="No bookings" width={"10%"} />
          <h2 className={styles.h2}>No bookings found !</h2>
        </center>
      )}

      <h2 className={styles.booking_headings}>Previous Bookings</h2>
      {props.prevBookings.length > 0 ? (
        <Row>
          {props.prevBookings.map((prevBooking) => {
            return (
              <Col lg="4" md="6" key={prevBooking._id}>
                <Wrapper>
                  <BookingsCard booking={prevBooking} />
                </Wrapper>
              </Col>
            );
          })}
        </Row>
      ) : (
        <center>
          <img src="images/no_bookings.png" alt="No bookings" width={"10%"} />
          <h2 className={styles.h2}>No bookings found !</h2>
        </center>
      )}

      <h2 className={styles.booking_headings}>Cancelled Bookings</h2>
      {props.cancelledBookings.length > 0 ? (
        <Row>
          {props.cancelledBookings.map((cancelledBooking) => {
            return (
              <Col lg="4" md="6" key={cancelledBooking._id}>
                <Wrapper>
                  <BookingsCard booking={cancelledBooking} cancelled={true} />
                </Wrapper>
              </Col>
            );
          })}
        </Row>
      ) : (
        <center>
          <img src="images/no_bookings.png" alt="No bookings" width={"10%"} />
          <h2 className={styles.h2}>No bookings found !</h2>
        </center>
      )}
    </Fragment>
  );
};

export default BookingsItems;

const Wrapper = styled.section`
  margin: 6% auto;
  width: 90%;
`;
