import React from "react";
import { Button, Modal, ModalBody, ModalFooter, Row, Col } from "reactstrap";
import styles from "./BookingModal.module.css";

function BookingModal(props) {
  return (
    <div>
      <Modal
        isOpen={props.modal}
        toggle={props.toggle}
        centered={true}
        size="lg"
        backdrop
        fade
        scrollable
      >
        <ModalBody>
          <div className={styles.modal_heading}>
            <img src="images/ticket.png" alt="" width={"13%"} />
            <center className={styles.modal_h1}>
              <h1>Booking Details</h1>
            </center>
          </div>
          <hr />
          <Row>
            <Col>
              <h5 className={styles.modal_h5}>
                Source : {props.bookingDetails.TripDetails[0].src}
              </h5>
            </Col>
            <Col>
              <h5 className={styles.modal_h5}>
                Destination : {props.bookingDetails.TripDetails[0].dest}
              </h5>
            </Col>
          </Row>
          <Row>
            <Col>
              <h5 className={styles.modal_h5}>
                Date : {new Date(props.bookingDetails.date).toDateString()}
              </h5>
            </Col>
            <Col>
              <div style={{ display: "flex" }}>
                <h5 className={styles.modal_h5}>
                  Time : {props.bookingDetails.TripDetails[0].start_time}
                </h5>

                <h5 className={styles.modal_h5}>
                  Duration : {props.bookingDetails.TripDetails[0].duration}
                </h5>
              </div>
            </Col>
          </Row>
          <Row>
            <Col>
              <h5 className={styles.modal_h5}>
                Travels : {props.bookingDetails.travels_name}
              </h5>
            </Col>
            <Col>
              <h5 className={styles.modal_h5}>
                Total Price : Rs. {props.bookingDetails.total_price}
              </h5>
            </Col>
          </Row>

          <hr className={styles.modal_hr} />

          <h5 className={styles.modal_pass_heading}>Passenger Details: </h5>

          {props.bookingDetails.passenger_details.map((details) => {
            return (
              <Row key={details.seatNo}>
                <Col>
                  <h5 className={styles.pass_h5}>Seat no : {details.seatNo}</h5>
                </Col>
                <Col>
                  <h5 className={styles.pass_h5}>Name : {details.name}</h5>
                </Col>
                <Col>
                  <h5 className={styles.pass_h5}>Age : {details.age}</h5>
                </Col>
              </Row>
            );
          })}
        </ModalBody>
        <ModalFooter>
          <Button color="secondary" onClick={props.toggle}>
            Cancel
          </Button>
        </ModalFooter>
      </Modal>
    </div>
  );
}

export default BookingModal;
