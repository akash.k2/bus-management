import {
  Card,
  CardHeader,
  Row,
  Col,
  CardBody,
  CardTitle,
  Button,
} from "reactstrap";
import { BsFillBusFrontFill } from "react-icons/bs";
import styled from "styled-components";
import axios from "axios";
import { CANCEL_BOOKING_SUCCESS, toastConfig } from "../../Utils/constants";
import Swal from "sweetalert2";
import { ToastContainer, toast } from "react-toastify";
import BookingModal from "./BookingModal";
import { useState } from "react";
import styles from "./BookingsCard.module.css";
import { forbiddenCheck } from "../../Utils/forbiddenCheck";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";

const BookingsCard = (props) => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const booking_id = props.booking._id;
  const [modal, setModal] = useState(false);
  const toggle = () => setModal((modal) => !modal);

  const cancelTicketHandler = async () => {
    const userRes = await Swal.fire({
      title: "Do you want to cancel the booking?",
      text: "You won't be able to revert this!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, cancel it!",
    });
    if (userRes.isConfirmed) {
      try {
        const res = await axios.post("/cancel-booking", { booking_id });
        if (
          res.status === CANCEL_BOOKING_SUCCESS.status &&
          res.data.message === CANCEL_BOOKING_SUCCESS.message
        ) {
          const userAlert = await Swal.fire(
            "Cancelled!",
            "Your booking has been cancelled.",
            "success"
          );
          if (userAlert.isConfirmed) {
            window.location.reload();
          }
        }
      } catch (err) {
        forbiddenCheck(err, dispatch, navigate);
        toast.error(err.response.data.message, toastConfig);
      }
    }
  };

  return (
    props.booking.TripDetails.length > 0 && (
      <Card>
        <CardHeader className={styles.card_header}>
          <h5>
            <center>{new Date(props.booking.date).toDateString()}</center>
          </h5>
        </CardHeader>

        <CardBody>
          {props.cancelled && (
            <center>
              <img src="/images/cancelled.png" style={{ width: "70%" }}></img>
            </center>
          )}
          <Row className={styles.row_3}>
            <Col>
              <BsFillBusFrontFill size="3rem" />
            </Col>
            <Col>
              <CardTitle tag="h4">{props.booking.TripDetails[0].src}</CardTitle>
            </Col>
            <Col>
              <CardTitle tag="h4">
                {props.booking.TripDetails[0].dest}
              </CardTitle>
            </Col>
          </Row>
          <Row className={styles.row_4}>
            <Col xs="2">
              <center>
                <CardTitle tag="h5">
                  ({props.booking.passenger_details.length})
                </CardTitle>
              </center>
            </Col>
            <Col>
              <center>
                <CardTitle tag="h5">
                  Time : {props.booking.TripDetails[0].start_time}
                </CardTitle>
              </center>
            </Col>
            <Col>
              <center>
                <CardTitle tag="h5">{props.booking.travels_name}</CardTitle>
              </center>
            </Col>
          </Row>
          <Wrapper>
            <Button color="warning" onClick={toggle}>
              View Details
            </Button>
            {props.isCancel && (
              <Button color="danger" onClick={cancelTicketHandler}>
                Cancel Booking
              </Button>
            )}
          </Wrapper>
        </CardBody>
        {modal && (
          <BookingModal
            modal={modal}
            toggle={toggle}
            bookingDetails={props.booking}
          />
        )}
        <ToastContainer />
      </Card>
    )
  );
};

export default BookingsCard;

const Wrapper = styled.section`
  align-items: center;
  display: flex;
  justify-content: space-evenly;
  margin-top: 7%;
`;
