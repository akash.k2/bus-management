import React, { Fragment, useEffect, useState } from "react";

import { useSelector } from "react-redux";
import axios from "axios";
import { toast, ToastContainer } from "react-toastify";
import { FETCH_SUCCESS, FORBIDDEN, toastConfig } from "../../Utils/constants";
import BookingsItems from "../components/BookingsItems";

import Unauthorized from "../../Common/pages/Unauthorized";

import { useErrorBoundary, withErrorBoundary } from "react-error-boundary";

const MyBookings = () => {
  const { showBoundary } = useErrorBoundary();
  const [prevBookings, setPrevBookings] = useState([]);
  const [currBookings, setCurrBookings] = useState([]);
  const [cancelledBookings, setCancelledBookings] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const user_id = useSelector((state) => state.user.id);

  useEffect(() => {
    const getBookings = async () => {
      try {
        setIsLoading(true);
        const res = await axios.get(`/view-bookings/${user_id}`);
        if (res.status === FETCH_SUCCESS.status) {
          setPrevBookings(res.data.prevBookings);
          setCurrBookings(res.data.currBookings);
          setCancelledBookings(res.data.cancelledBookings);
        }
      } catch (err) {
        if (
          err.response.status === FORBIDDEN.status &&
          err.response.data.message === FORBIDDEN.message
        ) {
          showBoundary(err);
          return;
        } else {
          toast.error(err.response.data.message, toastConfig);
        }
      } finally {
        setIsLoading(false);
      }
    };
    getBookings();
  }, []);

  return (
    <Fragment>
      {isLoading ? (
        <center>
          <img
            src="images/bus-gif.gif"
            alt=""
            width={"35%"}
            style={{ marginTop: "4%" }}
          />
        </center>
      ) : (
        <Fragment>
          <BookingsItems
            prevBookings={prevBookings}
            currBookings={currBookings}
            cancelledBookings={cancelledBookings}
          />
          <ToastContainer />
        </Fragment>
      )}
    </Fragment>
  );
};

const MyBookingsWithErrorBoundary = withErrorBoundary(MyBookings, {
  fallbackRender: (props) => {
    return <Unauthorized />;
  },
});

export default MyBookingsWithErrorBoundary;
