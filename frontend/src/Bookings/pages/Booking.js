import React, { useEffect, useState } from "react";
import { useSearchParams } from "react-router-dom";
import axios from "axios";
import { toast, ToastContainer } from "react-toastify";
import SeatPicker from "../components/SeatPicker";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { BOOKING_SUCCESS, toastConfig, FORBIDDEN } from "../../Utils/constants";

import Swal from "sweetalert2";
import { useErrorBoundary, withErrorBoundary } from "react-error-boundary";
import Unauthorized from "../../Common/pages/Unauthorized";
import { removeSearchDetails } from "../../store/slices/searchSlice";

const Booking = () => {
  const { showBoundary } = useErrorBoundary();
  const [searchParams] = useSearchParams();

  const [bookedSeats, setBookedSeats] = useState([]);

  const navigate = useNavigate();

  const [isLoading, setIsLoading] = useState(false);
  const dispatch = useDispatch();

  const trip_id = searchParams.get("trip_id");
  const date = searchParams.get("date");

  const trips = useSelector((state) => state.trip.trips);

  const currentTrip = [...trips].find((trip) => {
    return trip._id === trip_id;
  });

  const user_id = useSelector((state) => state.user.id);

  const submitHandler = async (details) => {
    const passenger_details = details.passDetails;
    const seatNumbers = details.seatNumbers;
    const total_price = details.totalPrice;
    const travels_name = currentTrip.travels_name;

    const bookingDetails = {
      user_id,
      date,
      trip_id,
      passenger_details,
      seatNumbers,
      total_price,
      travels_name,
    };

    try {
      const res = await axios.post("/book-trip", bookingDetails);
      if (
        res.status === BOOKING_SUCCESS.status &&
        res.data.message === BOOKING_SUCCESS.message
      ) {
        const userAlert = await Swal.fire(
          "Success!",
          "Tickets booked successfully!",
          "success"
        );
        if (userAlert.isConfirmed) {
          dispatch(removeSearchDetails());
          navigate("/my-bookings");
        }
      }
    } catch (err) {
      if (
        err.response.status === FORBIDDEN.status &&
        err.response.data.message === FORBIDDEN.message
      ) {
        showBoundary(err);
        return;
      } else {
        toast.error(err.response.data.message, toastConfig);
      }
    }
  };

  useEffect(() => {
    const getBookedSeats = async () => {
      setIsLoading(true);
      try {
        const res = await axios.get(
          `/get-bookedseats?trip_id=${trip_id}&date=${date}`
        );
        setBookedSeats(res.data.bookedSeats);
      } catch (err) {
        if (
          err.response.status === FORBIDDEN.status &&
          err.response.data.message === FORBIDDEN.message
        ) {
          showBoundary(err);
          return;
        } else {
          toast.error(err.response.data.message, toastConfig);
          navigate("/trips");
          return;
        }
      } finally {
        setIsLoading(false);
      }
    };
    getBookedSeats();
  }, []);

  if (!currentTrip) {
    navigate("/trips");
    return;
  }

  const seat_cap = currentTrip.seat_cap;

  const price = currentTrip.price;

  return (
    <div>
      {isLoading ? (
        <center>
          <img
            src="images/bus-gif.gif"
            alt=""
            width={"25%"}
            style={{ marginTop: "4%" }}
          />
        </center>
      ) : (
        <SeatPicker
          bookedSeats={bookedSeats}
          seat_cap={seat_cap}
          price={price}
          onSubmit={submitHandler}
        />
      )}
      <ToastContainer />
    </div>
  );
};

const BookingWithErrorBoundary = withErrorBoundary(Booking, {
  fallbackRender: (props) => {
    return <Unauthorized />;
  },
});

export default Booking;
